import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:mysmartfarm/screens/SplashPage.dart';

final navigatorKey = GlobalKey<NavigatorState>();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyApp createState() => _MyApp();
}

class _MyApp extends State<MyApp> {
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xff72cb49),
        buttonColor: Color(0xffff8a5b),
        primarySwatch: Colors.deepPurple,
        accentColor: Colors.orange,
        cursorColor: Colors.orange,
        textTheme: TextTheme(
            bodyText1: TextStyle(fontFamily: "Sarabun", color: Colors.black54)),
      ),
      title: 'สวนทุเรียน',
      home: SplashPage(),
      initialRoute: '/',
    );
  }
}
