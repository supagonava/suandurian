import "dart:io";

import "package:flutter/material.dart";
import "package:shared_preferences/shared_preferences.dart";
import "package:http/http.dart" as http;
import "dart:convert" as convert;

class Config {
  static String host = "http://192.168.1.33/mysmartfarm_web/";
  // static String host = "https://devrmutt.southeastasia.cloudapp.azure.com/";
  static String apiPath = host + "api/";
  static String storagePath = host + "public/storage/";
  static String news = host + "feed";

  static String key = "AIzaSyB5eiAIhp5Kg53iwEj4ytwP9";
  static const String log = "Supakorn .";

  static Map api = {
    "assingRole": apiPath + "role/assing",
    "auth-update": apiPath + "user/update",
    "clearRole": apiPath + "role/clear",
    "create-farm": apiPath + "create-farm",
    "delete-farm": apiPath + "delete-farm/",
    "delete-durian": apiPath + "durian/delete",
    "delete-durian-detail": apiPath + "durian/detail/delete",
    "durian-store": apiPath + "durian/store",
    "durian-update": apiPath + "durian/update",
    "durian-detail-store": apiPath + "durian/detail/create",
    "durian-detail-update": apiPath + "durian/detail/update",
    "durian-get": apiPath + "durian",
    "detail-get": apiPath + "durian/detail",
    "get-chart": apiPath + "chart/device/",
    "get-device-qr": apiPath + "get-device-qr/",
    "get-devices": apiPath + "get-devices",
    "get-farm": apiPath + "get-farm",
    "get-notifications": apiPath + "user/get-notification",
    "get-user-by-farm": apiPath + "user/get-by-farm/",
    "get-user-qr": apiPath + "get-user-qr/",
    "login": apiPath + "login",
    "read-notification": apiPath + "user/read-notification",
    "test-req": apiPath + "test-req",
    "update-device": apiPath + "update-device/",
    "update-farm": apiPath + "update-farm/",
    "validate": apiPath + "validate",
  };

  static myPost(var data, String url) async {
    print("${Config.log}post to url : $url");
    print("${Config.log}post data : ${data.toString()}");
    var body = convert.jsonEncode(data);
    var response;
    Map<String, dynamic> returnData = {"status": false, "data": null};
    try {
      response = await http.post(url,
          headers: {"Content-Type": "application/json"}, body: body);
      print("${Config.log}response code : ${response.statusCode}");
      print("${Config.log}response body : ${response.body}");

      if (response.statusCode == 200) {
        if (response.body.toString() != "null") {
          returnData["status"] = true;
          returnData["data"] = convert.jsonDecode(response.body);
        }
      } else if (response.statusCode == 204) {
        returnData["status"] = true;
        returnData["data"] = "success";
      }
    } catch (e) {
      returnData["data"] = e.toString();
    }
    print(Config.log + returnData.toString());
    return returnData;
  }

  static Future<Map<String, dynamic>> myMultipartReq(
      {String url, Map data}) async {
    var req = http.MultipartRequest("POST", Uri.parse(url));
    data.forEach((key, value) async {
      if (value.runtimeType.toString().contains("_InternalLinkedHashMap")) {
        value.forEach((key2, value2) async {
          if (value2.runtimeType.toString().contains("_File")) {
            File file = value2;
            req.files.add(
                await http.MultipartFile.fromPath("$key[$key2]", file.path));
          } else {
            req.fields["$key[$key2]"] = value2.toString();
          }
        });
      } else {
        if (value.runtimeType.toString().contains("_File")) {
          File file = value;
          req.files.add(await http.MultipartFile.fromPath("image", file.path));
        } else {
          req.fields[key] = value;
        }
      }
    });
    print(Config.log + Uri.parse(url).toString());
    print(Config.log + req.fields.toString());
    print(Config.log + req.files.toString());
    Map<String, dynamic> returnData = {"status": false, "data": null};
    try {
      var response = await req.send();
      var body = await response.stream.bytesToString();
      print("${Config.log}response code : ${response.statusCode}");
      print("${Config.log}response body : $body");
      if (response.statusCode == 200 || response.statusCode == 204) {
        returnData['status'] = true;
        returnData["data"] = response.stream;
        return returnData;
      } else {
        return returnData;
      }
    } catch (e) {
      print(Config.log + e.toString());
      return returnData;
    }
  }

  static getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get("token");
    return token;
  }

  static setToken(newToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("token", newToken);
  }

  static buildShowDialog({BuildContext context, message}) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: Text("แจ้งเตือน"),
        content: Text("$message"),
        actions: [
          FlatButton(
              onPressed: () => Navigator.of(context).pop(), child: Text("ตกลง"))
        ],
      ),
    );
  }

  static TextStyle defaultTextStyle(
      {Color color, String fontFamily, double fontSize}) {
    return TextStyle(
        color: color ?? Colors.black,
        fontFamily: fontFamily ?? "sarabun",
        fontSize: fontSize ?? 16);
  }
}
