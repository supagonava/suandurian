import "package:flutter/material.dart";
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysmartfarm/Config.dart';
import 'package:mysmartfarm/models/Farm.dart';
import "package:mysmartfarm/models/Iot.dart";
import "package:font_awesome_flutter/font_awesome_flutter.dart";
import 'package:mysmartfarm/screens/Loading.dart';
import "SubmitIotPage.dart";
import '../models/User.dart';

class IotManagementPage extends StatefulWidget {
  final Farm farm;
  IotManagementPage(this.farm);
  @override
  _IotManagementPageState createState() => _IotManagementPageState();
}

class _IotManagementPageState extends State<IotManagementPage> {
  Map<String, String> toggles = {};
  bool isLoad = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: ButtonTheme(
        height: 80,
        child: FlatButton.icon(
          color: Theme.of(context).buttonColor,
          onPressed: () {
            if (!(User.localUser.isManager == 1 ||
                User.localUser.isOwner == 1 ||
                User.localUser.isAdmin == 1)) {
              return Fluttertoast.showToast(
                  msg: "คุณไม่มีสิทธิ์ในการเรียกใช้งานฟังก์ชั่นดังกล่าว");
            }
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) =>
                    SubmitIOT(farm: widget.farm)));
          },
          icon: FaIcon(FontAwesomeIcons.plus, color: Colors.white),
          label: Text("เพิ่มอุปกรณ์",
              style: TextStyle(
                  color: Colors.white, fontFamily: "Sarabun", fontSize: 20)),
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "จัดการอุปกรณ์",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
      ),
      body: isLoad
          ? LoadingScreen()
          : SingleChildScrollView(
              child: Column(
                children: IOT.iotList.isEmpty
                    ? [
                        Center(
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.7,
                            child: Center(
                                child: Text("ไม่มีอุปกรณ์ที่ติดตั้ง",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: "Sarabun"))),
                          ),
                        )
                      ]
                    : List.generate(IOT.iotList.length, (index) {
                        toggles["status$index"] = IOT.iotList[index].status;
                        return Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            width: double.maxFinite,
                            child: Card(
                              elevation: 1,
                              child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                    padding:
                                        const EdgeInsets.only(left: 10, top: 5),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Align(
                                                alignment: Alignment.centerLeft,
                                                child: CircleAvatar(
                                                  backgroundColor: IOT
                                                              .iotList[index]
                                                              .status ==
                                                          "on"
                                                      ? Theme.of(context)
                                                          .primaryColor
                                                      : Colors.grey[300],
                                                  child: Image.asset(
                                                    "assets/images/iotLogo.png",
                                                    scale: 1,
                                                  ),
                                                )),
                                            Padding(
                                                padding:
                                                    EdgeInsets.only(left: 30),
                                                child: Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                      "${IOT.iotList[index].name}",
                                                      style: TextStyle(
                                                        color: Colors.black87,
                                                        fontFamily: "Sarabun",
                                                        fontSize: 18,
                                                      )),
                                                ))
                                          ],
                                        ),
                                        Divider(),
                                        Row(children: <Widget>[
                                          Text("ตัวแปร : ",
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16,
                                                  fontFamily: "Sarabun")),
                                          Text("${IOT.iotList[index].variable}",
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16,
                                                  fontFamily: "Sarabun"))
                                        ]),
                                        Row(children: <Widget>[
                                          Text("สถานะ : ",
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16,
                                                  fontFamily: "Sarabun")),
                                          InkWell(
                                            onTap: () async {
                                              if (!(User.localUser.isManager ==
                                                      1 ||
                                                  User.localUser.isOwner == 1 ||
                                                  User.localUser.isAdmin ==
                                                      1)) {
                                                return Fluttertoast.showToast(
                                                    msg:
                                                        "คุณไม่มีสิทธิ์ในการเรียกใช้งานฟังก์ชั่นดังกล่าว");
                                              }
                                              setState(() {
                                                isLoad = true;
                                                IOT.iotList[index].status ==
                                                        "on"
                                                    ? IOT.iotList[index]
                                                        .status = "off"
                                                    : IOT.iotList[index]
                                                        .status = "on";
                                              });

                                              bool isUpdate = await IOT
                                                  .iotList[index]
                                                  .update();
                                              if (isUpdate) {
                                                setState(() {
                                                  toggles["status$index"] =
                                                      IOT.iotList[index].status;
                                                });
                                                Fluttertoast.showToast(
                                                    msg:
                                                        "ปรับสถานะเป็น ${IOT.iotList[index].status} สำเร็จ");
                                              } else {
                                                setState(() {
                                                  IOT.iotList[index].status ==
                                                          "on"
                                                      ? IOT.iotList[index]
                                                          .status = "off"
                                                      : IOT.iotList[index]
                                                          .status = "on";
                                                  toggles["status$index"] =
                                                      IOT.iotList[index].status;
                                                });
                                                Fluttertoast.showToast(
                                                    msg:
                                                        'ไม่สามารถ ${IOT.iotList[index].status} ได้');
                                              }
                                              setState(() {
                                                isLoad = false;
                                              });
                                            },
                                            child: FaIcon(
                                              toggles["status$index"] == 'on'
                                                  ? FontAwesomeIcons.toggleOn
                                                  : FontAwesomeIcons.toggleOff,
                                              color: toggles["status$index"] ==
                                                      'on'
                                                  ? Colors.amber[600]
                                                  : Colors.grey[400],
                                              size: 28,
                                            ),
                                          ),
                                          Text(
                                              toggles["status$index"] == "on"
                                                  ? "  เปิด"
                                                  : "  ปิด",
                                              style: TextStyle(
                                                  color: Colors.black54,
                                                  fontSize: 16,
                                                  fontFamily: "Sarabun")),
                                        ]),
                                        (User.localUser.isManager == 1 ||
                                                User.localUser.isOwner == 1 ||
                                                User.localUser.isAdmin == 1)
                                            ? ButtonTheme(
                                                minWidth: double.infinity,
                                                child: FlatButton.icon(
                                                    color: Colors.redAccent,
                                                    onPressed: () async {
                                                      bool confirm =
                                                          await showDialog(
                                                              context: context,
                                                              builder:
                                                                  (BuildContext
                                                                      context) {
                                                                return AlertDialog(
                                                                  actions: [
                                                                    RaisedButton(
                                                                        color: Colors
                                                                            .blueGrey,
                                                                        child: Text(
                                                                            "ยกเลิก"),
                                                                        onPressed:
                                                                            () =>
                                                                                Navigator.of(context).pop(false)),
                                                                    RaisedButton(
                                                                        color: Colors
                                                                            .blueGrey,
                                                                        child: Text(
                                                                            "ตกลง"),
                                                                        onPressed:
                                                                            () =>
                                                                                Navigator.of(context).pop(true))
                                                                  ],
                                                                  title: Text(
                                                                      "แจ้งเตือน"),
                                                                  content: Text(
                                                                      "ยืนยันที่จะดำเนินการ"),
                                                                );
                                                              });
                                                      if (confirm != null &&
                                                          confirm) {
                                                        setState(() {
                                                          isLoad = true;
                                                        });
                                                        String token =
                                                            await Config
                                                                .getToken();
                                                        Map data = {
                                                          "token": token,
                                                          "Device": {
                                                            "farm_id": null
                                                          }
                                                        };
                                                        var res = await Config.myPost(
                                                            data,
                                                            Config.api[
                                                                    'update-device'] +
                                                                IOT
                                                                    .iotList[
                                                                        index]
                                                                    .id
                                                                    .toString());
                                                        if (res['message'] ==
                                                            null) {
                                                          Fluttertoast.showToast(
                                                              msg:
                                                                  "นำออกจากสวนแล้ว");
                                                          setState(() {
                                                            IOT.iotList.removeWhere(
                                                                (element) =>
                                                                    element
                                                                        .id ==
                                                                    IOT
                                                                        .iotList[
                                                                            index]
                                                                        .id);
                                                            Navigator.of(
                                                                    context)
                                                                .pop();
                                                          });
                                                        } else {
                                                          Fluttertoast.showToast(
                                                              msg:
                                                                  "ไม่สามารถดำเนินการได้โปรดตรวจสอบการเชื่อมต่อ");
                                                        }
                                                        setState(() {
                                                          isLoad = false;
                                                        });
                                                      }
                                                    },
                                                    icon: Icon(
                                                        FontAwesomeIcons.times,
                                                        color: Colors.white),
                                                    label: Text("ถอนการติดตั้ง",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white))),
                                              )
                                            : Container()
                                      ],
                                    )),
                              ),
                            ));
                      }),
              ),
            ),
    );
  }
}
