import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Durian.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mysmartfarm/screens/Loading.dart';

import '../Config.dart';

// ignore: must_be_immutable
class FormDurian extends StatefulWidget {
  Farm farm;
  Durian durian;
  FormDurian({this.farm, this.durian});
  @override
  _FormDurianState createState() => _FormDurianState();
}

class _FormDurianState extends State<FormDurian> {
  Durian durian = Durian();
  TextEditingController name = TextEditingController();
  bool isLoad = false;
  File image;
  @override
  void initState() {
    super.initState();
    durian.farmId = widget.farm.id;
    if (widget.durian != null) {
      durian = widget.durian;
      name.text = durian.name;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "เพิ่ม/แก้ไข ต้นทุเรียน",
            style: TextStyle(color: Colors.white, fontFamily: "sarabun"),
          ),
          iconTheme: IconThemeData(color: Colors.white),
          centerTitle: true,
        ),
        body: isLoad
            ? LoadingScreen()
            : Center(
                child: Container(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Card(
                            child: MaterialButton(
                              onPressed: () async {
                                File oldImage = durian.image;
                                try {
                                  // ignore: deprecated_member_use
                                  durian.image = await ImagePicker.pickImage(
                                      source: ImageSource.gallery);
                                  setState(() => durian.image);
                                } catch (e) {
                                  setState(() => durian.image = oldImage);
                                }
                              },
                              height: 250,
                              child: Center(
                                child: (() {
                                  if (durian.image != null) {
                                    return Image.file(durian.image);
                                  } else if (durian.imageUrl != null) {
                                    return Image.network(durian.imageUrl);
                                  } else {
                                    return Wrap(children: [
                                      Icon(
                                        FontAwesomeIcons.fileImage,
                                        color: Colors.black,
                                      ),
                                      Text(
                                        " เลือกรูปภาพ",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: "sarabun"),
                                      )
                                    ]);
                                  }
                                }()),
                              ),
                            ),
                          ),
                          Divider(),
                          TextField(
                            controller: name,
                            onChanged: (value) =>
                                setState(() => durian.name = value),
                            decoration: InputDecoration(
                                icon: Text("ชื่อต้นทุเรียน"),
                                hintText: "เช่น ต้นทุเรียนยามรุ่ง"),
                          ),
                          MaterialButton(
                            minWidth: double.infinity,
                            color: Theme.of(context).primaryColor,
                            onPressed: () async {
                              if (durian.name == null ||
                                  (durian.image == null &&
                                      durian.imageUrl == null)) {
                                return Fluttertoast.showToast(
                                    msg: "รูปภาพและชื่อต้นทุเรียนห้ามว่าง");
                              }
                              setState(() => isLoad = true);
                              try {
                                bool saved = await durian.save(
                                    isUpdate:
                                        widget.durian == null ? false : true);
                                if (saved) {
                                  Navigator.of(context).pop();
                                  Fluttertoast.showToast(msg: "บันทึกสำเร็จ");
                                } else {
                                  Fluttertoast.showToast(
                                      msg: "บันทึกไม่สำเร็จ");
                                }
                              } catch (e) {
                                Config.buildShowDialog(
                                    context: context, message: "$e");
                              }

                              setState(() => isLoad = false);
                            },
                            child: Wrap(children: [
                              Icon(
                                FontAwesomeIcons.save,
                                color: Colors.white,
                              ),
                              Text(
                                " บันทึก",
                                style: TextStyle(
                                    color: Colors.white, fontFamily: "sarabun"),
                              )
                            ]),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
