import 'package:flutter/material.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/widgets/MyFarmListbox.dart';

import 'AddFarmPage.dart';

class MyHome extends StatefulWidget {
  const MyHome({
    Key key,
  }) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  bool isLoading = true;
  @override
  void initState() {
    super.initState();
    myAsyncInit();
  }

  void myAsyncInit() async {
    await Farm.getFarm();
    if (Farm.farmList.length == 0) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("แจ้งเตือน"),
              content: Text("คุณยังไม่ได้เพิ่มสวนทุเรียน"),
              actions: <Widget>[
                FlatButton(
                  child: Text("ตกลง"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Container addfarmBtn() {
      return Container(
        color: Colors.transparent,
        child: Container(
          height: 50,
          width: 315,
          decoration: BoxDecoration(
            color: Color(0xff707070),
            border: Border.all(
              width: 2.00,
              color: Colors.transparent,
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(0.00, 3.00),
                color: Color(0xff000000).withOpacity(0.16),
                blurRadius: 6,
              ),
            ],
            borderRadius: BorderRadius.circular(9.00),
          ),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => AddFarmPage()));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.add,
                  color: Colors.white,
                ),
                Text(
                  '  เพิ่มสวนทุเรียน',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Sarabun",
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }

    return Container(
      color: Color(0xffE8E8E8),
      child: ListView(
        children: <Widget>[
          MyFarmListbox(),
          Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              addfarmBtn(),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
