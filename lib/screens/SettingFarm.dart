import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/Iot.dart';
import 'package:mysmartfarm/models/User.dart';
import 'package:mysmartfarm/mylib/CustomPlacePicker.dart';
import 'package:mysmartfarm/screens/DashBoardPage.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import 'package:mysmartfarm/screens/MainPage.dart';
import 'package:place_picker/entities/localization_item.dart';
import '../widgets/MyTextField.dart';
import '../widgets/MyBtn.dart';

// ignore: must_be_immutable
class SettingFarmPage extends StatefulWidget {
  Farm farm;
  List<IOT> iots;
  SettingFarmPage(this.farm, this.iots);
  @override
  _SettingFarmPageState createState() => _SettingFarmPageState();
}

class _SettingFarmPageState extends State<SettingFarmPage> {
  bool isLoad = false;
  MyTextField farmNameTextField =
          MyTextField(txtFieldString: "ชื่อสวน", inputType: TextInputType.text),
      farmSizeTextField =
          MyTextField(txtFieldString: "ขนาด", inputType: TextInputType.text);
  List variableList = IOT.iotList;
  Map suggestionsValues = {};
  @override
  void initState() {
    super.initState();
    farmNameTextField.txtController.text = widget.farm.name;
    farmSizeTextField.txtController.text = widget.farm.size.toString();
    for (var device in widget.iots) {
      suggestionsValues[device.id] = {
        "id": device.id,
        "min": MyTextField(
          inputType: TextInputType.number,
          txtFieldString: '',
        ),
        "max": MyTextField(
          inputType: TextInputType.number,
          txtFieldString: '',
        )
      };
      suggestionsValues[device.id]['min'].txtController.text =
          device.min.toString();
      suggestionsValues[device.id]['max'].txtController.text =
          device.max.toString();
    }
  }

  void showInfoFlushbar(BuildContext context, String text) {
    Flushbar(
      title: 'แจ้งเตือน',
      message: text,
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Color(0xff75C46B),
      ),
      leftBarIndicatorColor: Color(0xff75C46B),
      duration: Duration(seconds: 3),
    ).show(context);
  }

  updateFarm() async {
    if (farmNameTextField.txtController.text.isNotEmpty &&
        farmSizeTextField.txtController.text.isNotEmpty) {
      widget.farm.name = farmNameTextField.txtController.text;
      widget.farm.size = double.parse(farmSizeTextField.txtController.text);
      bool isUpdate = await widget.farm.update();
      if (isUpdate) {
        return true;
      } else {
        return false;
      }
    }
  }

  updateDevice() async {
    List<String> errors = [];
    try {
      for (var device in widget.iots) {
        // Fluttertoast.showToast(msg: suggestionsValues[device.id]['min'].txtController.text);
        device.min = double.parse(
            suggestionsValues[device.id]['min'].txtController.text);
        device.max = double.parse(
            suggestionsValues[device.id]['max'].txtController.text);
        bool isUpdate = await device.update();
        if (!isUpdate) {
          errors.add("ล้มเหลวในการอัปเดต ${device.variable}\n");
        }
      }
      if (errors.isNotEmpty) {
        Fluttertoast.showToast(msg: "$errors");
      } else {
        Fluttertoast.showToast(msg: "อัปเดตข้อมูลทั้งหมดแล้ว");
        Navigator.of(context).pop();
        Navigator.of(context).pop();
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => DashboardPage(widget.farm)));
      }
    } catch (e) {
      Fluttertoast.showToast(msg: '$e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              "ตั้งค่าสวน",
              style: TextStyle(
                  fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
            ),
            centerTitle: true,
            backgroundColor: Color(0xff72cb49),
            iconTheme: IconThemeData(color: Colors.white),
          ),
          body: isLoad
              ? LoadingScreen()
              : Container(
                  color: Color(0xffE8E8E8),
                  child: ListView(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(30),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("ชื่อสวน",
                                style: TextStyle(
                                  fontFamily: "Sarabun",
                                  fontSize: 16,
                                  color: Color(0xff707070),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            farmNameTextField,
                            SizedBox(
                              height: 20,
                            ),
                            Text("ขนาด(แปลง)",
                                style: TextStyle(
                                  fontFamily: "Sarabun",
                                  fontSize: 16,
                                  color: Color(0xff707070),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            farmSizeTextField,
                            SizedBox(
                              height: 20,
                            ),
                            ButtonTheme(
                              height: 50,
                              child: FlatButton.icon(
                                  onPressed: () {
                                    showPlacePicker();
                                  },
                                  color: Theme.of(context).primaryColor,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  icon: FaIcon(FontAwesomeIcons.searchLocation,
                                      color: Colors.white),
                                  label: Text(
                                      widget.farm.location == null
                                          ? "ระบุตำแหน่ง"
                                          : "ระบุตำแหน่งแล้ว",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Sarabun"))),
                            ),
                            Divider(),
                            Text("ตั้งค่ามาตรฐานของตัวแปร",
                                style: TextStyle(
                                  fontFamily: "Sarabun",
                                  fontSize: 16,
                                  color: Color(0xff707070),
                                )),
                            SizedBox(
                              height: 5,
                            ),
                            Column(
                                children: variableList != null
                                    ? List.generate(variableList.length,
                                        (index) {
                                        return new Container(
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(25),
                                            color: Colors.lightGreen[200],
                                            boxShadow: [
                                              BoxShadow(
                                                offset: Offset(0.00, 3.00),
                                                color: Color(0xff000000)
                                                    .withOpacity(0.16),
                                                blurRadius: 6,
                                              ),
                                            ],
                                          ),
                                          margin: EdgeInsets.all(10),
                                          padding: EdgeInsets.all(5),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              Wrap(
                                                alignment: WrapAlignment.center,
                                                direction: Axis.horizontal,
                                                children: <Widget>[
                                                  Text(
                                                    "${variableList[index].name}\n\n มีค่ามาตรฐานที่ตั้งไว้อยู่ที่\n",
                                                    style: TextStyle(
                                                      fontFamily: "Sarabun",
                                                      fontSize: 16,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ],
                                              ),
                                              Container(
                                                padding: EdgeInsets.symmetric(
                                                    vertical: 5),
                                                child: Flex(
                                                  direction: Axis.horizontal,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Flexible(
                                                        flex: 1,
                                                        child: Container(
                                                            child: suggestionsValues[
                                                                variableList[
                                                                        index]
                                                                    .id]['min'])),
                                                    Flexible(
                                                      flex: 1,
                                                      child: Text(
                                                        " และ ",
                                                        style: TextStyle(
                                                          fontFamily: "Sarabun",
                                                          fontSize: 16,
                                                        ),
                                                      ),
                                                    ),
                                                    Flexible(
                                                        flex: 1,
                                                        child: Container(
                                                            child: suggestionsValues[
                                                                variableList[
                                                                        index]
                                                                    .id]['max'])),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      })
                                    : <Widget>[
                                        Container(
                                          padding: EdgeInsets.all(30),
                                          child: Center(
                                            child: Text(
                                              "ไม่มีตัวแปรในสวนนี้",
                                              style: TextStyle(
                                                fontFamily: "Sarabun",
                                                fontSize: 16,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ]),
                            SizedBox(
                              height: 15,
                            ),
                            MyBtn(
                                btnTxt: "อัปเดตข้อมูล",
                                txtColor: Colors.white,
                                btnColor: Color(0xff75c46b),
                                borderColor: Colors.transparent,
                                fn: () async {
                                  bool confirm = await showDialog<bool>(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        AlertDialog(
                                            title: Text("แจ้งเตือน"),
                                            content:
                                                Text("ยืนยันที่จะดำเนินการ"),
                                            actions: <Widget>[
                                          FlatButton(
                                              onPressed: () =>
                                                  Navigator.pop(context, false),
                                              child: Text("ยกเลิก")),
                                          FlatButton(
                                              onPressed: () =>
                                                  Navigator.pop(context, true),
                                              child: Text("ยืนยัน")),
                                        ]),
                                  );
                                  if (confirm != null && confirm) {
                                    setState(() {
                                      isLoad = true;
                                    });
                                    bool farmUpdate = await updateFarm();

                                    if (farmUpdate) {
                                      Farm.farmList.removeWhere((element) =>
                                          element.id == widget.farm.id);
                                      setState(() {
                                        Farm.farmList.add(widget.farm);
                                      });
                                      await updateDevice();
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "ไม่สามารถอัปเดตสวนนี้ได้ กรุณาตรวจสอบข้อมูลให้ถูกต้อง");
                                    }

                                    setState(() {
                                      isLoad = false;
                                    });
                                  }
                                }),
                            SizedBox(
                              height: 8,
                            ),
                            User.localUser.isOwner == 1
                                ? MyBtn(
                                    btnTxt: "ลบสวนนี้",
                                    txtColor: Colors.white,
                                    btnColor: Colors.red,
                                    borderColor: Colors.transparent,
                                    fn: () async {
                                      bool confirm = await showDialog<bool>(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            AlertDialog(
                                                title: Text("แจ้งเตือน"),
                                                content: Text(
                                                    "ยืนยันที่จะดำเนินการ"),
                                                actions: <Widget>[
                                              FlatButton(
                                                  onPressed: () =>
                                                      Navigator.pop(
                                                          context, false),
                                                  child: Text("ยกเลิก")),
                                              FlatButton(
                                                  onPressed: () =>
                                                      Navigator.pop(
                                                          context, true),
                                                  child: Text("ยืนยัน")),
                                            ]),
                                      );
                                      if (confirm) {
                                        setState(() {
                                          isLoad = true;
                                        });
                                        try {
                                          var isDelete =
                                              await widget.farm.delete();
                                          if (isDelete) {
                                            await Farm.getFarm();
                                            setState(() {
                                              isLoad = false;
                                            });
                                            Fluttertoast.showToast(
                                                msg: "ลบสำเร็จ");
                                            Navigator.pushAndRemoveUntil(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (BuildContext
                                                            context) =>
                                                        MainPage()),
                                                (route) => false);
                                          }
                                        } catch (e) {
                                          Fluttertoast.showToast(
                                              msg: e.toString());
                                        }
                                        setState(() {
                                          isLoad = false;
                                        });
                                      }
                                    })
                                : Container(),
                            SizedBox(
                              height: 8,
                            ),
                            MyBtn(
                                btnTxt: "ยกเลิก",
                                txtColor: Colors.white,
                                btnColor: Color(0xffff8a5b),
                                borderColor: Colors.transparent,
                                fn: () {
                                  Navigator.pop(context);
                                }),
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
    );
  }

  void showPlacePicker() async {
    Map<String, double> result =
        await Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CustomPlacePicker(
                  "AIzaSyB5eiAIhp5Kg53iwEj4ytwP9-x28tyjzf4",
                  localizationItem: LocalizationItem(
                      findingPlace: 'ค้นหา',
                      languageCode: 'th_TH',
                      unnamedLocation: 'แตะเพื่อเลือกพิกัดนี้',
                      nearBy: 'ตำแหน่งใกล้เคียง',
                      noResultsFound: 'ไม่พบรายการ',
                      tapToSelectLocation: ''),
                )));

    // Handle the result in your way
    // print(result['lat']);
    try {
      setState(() {
        widget.farm.location =
            FarmLatLng(lat: result['lat'], lng: result['lng']);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("แจ้งเตือน"),
              content: Text("ไม่ได้เลือกตำแหน่ง หมายเหตุ:$e"),
              actions: <Widget>[
                FlatButton(
                  child: Text("ตกลง"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }
}
