import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/User.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import 'package:mysmartfarm/screens/SubmitUserPage.dart';
import 'package:mysmartfarm/widgets/ConfirmDialog.dart';

class UserManagementPage extends StatefulWidget {
  final Farm farm;
  UserManagementPage(this.farm);
  @override
  _UserManagementPageState createState() => _UserManagementPageState();
}

class _UserManagementPageState extends State<UserManagementPage> {
  List<Widget> widgets = [];
  bool isLoad = false;

  @override
  void initState() {
    super.initState();
    myInitState();
  }

  void myInitState() async {
    setState(() => isLoad = true);
    // await widget.farm.getUser();
    setState(() => isLoad = false);
  }

  clearRole(User user) async {
    setState(() => isLoad = true);
    try {
      bool isSetRole = await user.clearRole(widget.farm.id);
      if (isSetRole) {
        Fluttertoast.showToast(msg: "ถอนสิทธิ์การใช้งานจากสวนสำเร็จ");
        Navigator.of(context).pop();
      } else {
        Fluttertoast.showToast(msg: "ถอนสิทธิ์การใช้งานจากสวนไม่สำเร็จ");
      }
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Text("พบข้อผิดพลาด"),
              children: [Text("$e")],
            );
          });
    }
    setState(() => isLoad = false);
  }

  setRole(User user) async {
    setState(() => isLoad = true);
    try {
      int roleId = user.isViewer == 1 ? 30 : 40;
      bool isSetRole = await user.updateRole(widget.farm.id, roleId);
      if (isSetRole) {
        Fluttertoast.showToast(msg: "ตั้งค่าบทบาทสำเร็จ");
      } else {
        Fluttertoast.showToast(msg: "ล้มเหลวในการตั้งค่าบทบาท");
      }
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Text("พบข้อผิดพลาด"),
              children: [Text("$e")],
            );
          });
    }
    setState(() => isLoad = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: ButtonTheme(
        height: 80,
        child: FlatButton.icon(
          color: Theme.of(context).buttonColor,
          onPressed: () {
            if (User.localUser.isAdmin == 1 || User.localUser.isOwner == 1) {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      SubmitUserPage(farm: widget.farm)));
            } else {
              Fluttertoast.showToast(msg: "คุณไม่มีสิทธิ์ใช้งานฟังก์ชั่นนี้");
            }
          },
          icon: FaIcon(FontAwesomeIcons.plus, color: Colors.white),
          label: Text("เพิ่มผู้ใช้งาน",
              style: TextStyle(
                  color: Colors.white, fontFamily: "Sarabun", fontSize: 20)),
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "จัดการผู้ใช้งาน",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
      ),
      body: isLoad
          ? LoadingScreen()
          : SingleChildScrollView(
              child: Column(
                children: User.userList
                        .where((element) => element.id != User.localUser.id)
                        .isEmpty
                    ? [
                        Center(
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.7,
                            child: Center(
                                child: Text("ไม่มีผู้ใช้งานที่มีบทบาทในสวนนี้",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: "Sarabun"))),
                          ),
                        )
                      ]
                    : User.userList
                        .where((element) => element.id != User.localUser.id)
                        .map((user) {
                        return Container(
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                            width: double.maxFinite,
                            child: Card(
                              elevation: 1,
                              child: Padding(
                                padding: EdgeInsets.all(7),
                                child: Stack(children: <Widget>[
                                  Align(
                                    alignment: Alignment.centerRight,
                                    child: Stack(
                                      children: <Widget>[
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: 10, top: 5),
                                            child: Column(
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 15.0),
                                                      child: Align(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          child: CircleAvatar(
                                                            backgroundColor: user
                                                                        .isManager ==
                                                                    1
                                                                ? Theme.of(
                                                                        context)
                                                                    .primaryColor
                                                                : user.isViewer ==
                                                                        1
                                                                    ? Colors.blue[
                                                                        300]
                                                                    : Colors.grey[
                                                                        400],
                                                            child: Image.asset(
                                                              "assets/images/farmerLogo.png",
                                                              scale: 1,
                                                            ),
                                                          )),
                                                    ),
                                                    Padding(
                                                        padding:
                                                            EdgeInsets.only(
                                                                left: 30),
                                                        child: Align(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          child: Text(
                                                              "${user.fullName ?? 'ไม่ระบุ'}",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black87,
                                                                fontFamily:
                                                                    "Sarabun",
                                                                fontSize: 20,
                                                              )),
                                                        ))
                                                  ],
                                                ),
                                                Divider(),
                                                Row(children: <Widget>[
                                                  Text("อีเมล์ : ",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Sarabun")),
                                                  Text(
                                                      "${user.email ?? 'ไม่ระบุ'}",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Sarabun"))
                                                ]),
                                                Row(children: <Widget>[
                                                  Text("เบอร์ : ",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Sarabun")),
                                                  Text(
                                                      "${user.phone ?? 'ไม่ระบุ'}",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Sarabun"))
                                                ]),
                                                Row(children: <Widget>[
                                                  Text("QR-CODE : ",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Sarabun")),
                                                  Text(
                                                      "${user.qrcode ?? 'ไม่ระบุ'}",
                                                      style: TextStyle(
                                                          color: Colors.black54,
                                                          fontSize: 16,
                                                          fontFamily:
                                                              "Sarabun"))
                                                ]),
                                                Divider(),
                                                user.isOwner == 1 ||
                                                        user.isAdmin == 1
                                                    ? Container()
                                                    : Row(children: <Widget>[
                                                        Text("ผู้จัดการสวน : ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 16,
                                                                fontFamily:
                                                                    "Sarabun")),
                                                        InkWell(
                                                          onTap: () {
                                                            setState(() async {
                                                              user.isManager =
                                                                  1;
                                                              user.isViewer = 0;
                                                              await setRole(
                                                                  user);
                                                            });
                                                          },
                                                          child: FaIcon(
                                                            user.isManager == 1
                                                                ? FontAwesomeIcons
                                                                    .toggleOn
                                                                : FontAwesomeIcons
                                                                    .toggleOff,
                                                            color: user.isManager ==
                                                                    1
                                                                ? Colors
                                                                    .amber[600]
                                                                : Colors
                                                                    .grey[400],
                                                            size: 28,
                                                          ),
                                                        ),
                                                      ]),
                                                user.isOwner == 1 ||
                                                        user.isAdmin == 1
                                                    ? Container()
                                                    : Row(children: <Widget>[
                                                        Text(
                                                            "ผู้สังเกตุการณ์ : ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 16,
                                                                fontFamily:
                                                                    "Sarabun")),
                                                        InkWell(
                                                          onTap: () {
                                                            setState(() async {
                                                              user.isManager =
                                                                  0;
                                                              user.isViewer = 1;
                                                              await setRole(
                                                                  user);
                                                            });
                                                          },
                                                          child: FaIcon(
                                                            user.isViewer == 1
                                                                ? FontAwesomeIcons
                                                                    .toggleOn
                                                                : FontAwesomeIcons
                                                                    .toggleOff,
                                                            color: user.isViewer ==
                                                                    1
                                                                ? Colors
                                                                    .amber[600]
                                                                : Colors
                                                                    .grey[400],
                                                            size: 28,
                                                          ),
                                                        ),
                                                      ]),
                                                user.isOwner == 1 ||
                                                        user.isAdmin == 1
                                                    ? Container()
                                                    : ButtonTheme(
                                                        minWidth:
                                                            double.infinity,
                                                        child: FlatButton.icon(
                                                            color: Colors
                                                                .redAccent,
                                                            onPressed:
                                                                () async {
                                                              bool confirm =
                                                                  await showDialog(
                                                                      context:
                                                                          context,
                                                                      builder:
                                                                          (BuildContext
                                                                              context) {
                                                                        return ConfirmDialog(
                                                                            context);
                                                                      });
                                                              if (confirm !=
                                                                      null &&
                                                                  confirm) {
                                                                await clearRole(
                                                                    user);
                                                              }
                                                            },
                                                            icon: Icon(
                                                                FontAwesomeIcons
                                                                    .times,
                                                                color: Colors
                                                                    .white),
                                                            label: Text(
                                                                "ถอนสิทธิ์การใช้งาน",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white))),
                                                      )
                                              ],
                                            ))
                                      ],
                                    ),
                                  )
                                ]),
                              ),
                            ));
                      }).toList(),
              ),
            ),
    );
  }
}
