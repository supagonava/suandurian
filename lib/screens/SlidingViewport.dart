import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';
import 'package:mysmartfarm/models/Iot.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import 'package:percent_indicator/percent_indicator.dart';
import '../Config.dart';

class SlidingViewport extends StatefulWidget {
  static TextEditingController showLength = TextEditingController();
  @override
  _SlidingViewportState createState() => _SlidingViewportState();
}

class _SlidingViewportState extends State<SlidingViewport> {
  bool isLoad = false;
  String header = "เลือกอุปกรณ์เพื่อดูประวัติ";
  double avg = 0, percent = 0.0;

  IOT selected = IOT();
  String selectedDate =
      "${DateTime.now().year}-${DateTime.now().month < 10 ? '0' + DateTime.now().month.toString() : DateTime.now().month}-${DateTime.now().day < 10 ? '0' + DateTime.now().day.toString() : DateTime.now().day}";
  String selectedType = "date";
  @override
  void initState() {
    super.initState();
    History.historyData.clear();
    SlidingViewport.showLength.text = 4.toString();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Color(0xff72cb49),
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            "ประวัติย้อนหลัง",
            style: TextStyle(
                fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
          ),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: Text("$header",
                        style: Config.defaultTextStyle(fontSize: 20))),
              ),
              Card(
                shadowColor: Colors.black,
                elevation: 5,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Column(
                    children: [
                      Wrap(
                          spacing: 4,
                          direction: Axis.horizontal,
                          children: IOT.iotList
                              .map((e) => Padding(
                                    padding: const EdgeInsets.all(2.0),
                                    child: MaterialButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(25))),
                                      elevation: 1.5,
                                      color: Theme.of(context).buttonColor,
                                      onPressed: () async {
                                        selected = e;
                                        await fetchChart();
                                      },
                                      child: Text(e.name,
                                          style: Config.defaultTextStyle(
                                              color: Colors.white,
                                              fontSize: 14)),
                                    ),
                                  ))
                              .toList())
                    ],
                  ),
                ),
              ),
              Card(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Wrap(
                  crossAxisAlignment: WrapCrossAlignment.center,
                  spacing: 5,
                  children: [
                    Text("เลือกเวลาที่จะแสดง:"),
                    MaterialButton(
                      color: Colors.blueAccent,
                      onPressed: () {
                        DatePicker.showDatePicker(context,
                            showTitleActions: true,
                            onChanged: (date) {}, onConfirm: (date) async {
                          setState(() => selectedDate =
                              "${date.year}-${date.month < 10 ? '0' + date.month.toString() : date.month}-${date.day < 10 ? '0' + date.day.toString() : date.day}");
                          selectedType = "date";
                          await fetchChart();
                        }, currentTime: DateTime.now(), locale: LocaleType.th);
                      },
                      child: Text("รายวัน",
                          style: Config.defaultTextStyle(color: Colors.white)),
                    ),
                    MaterialButton(
                      color: Colors.blueAccent,
                      onPressed: () {
                        showMonthPicker(
                          context: context,
                          firstDate: DateTime(DateTime.now().year - 1),
                          lastDate: DateTime.now(),
                          initialDate: DateTime.now(),
                          locale: Locale("th"),
                        ).then((date) async {
                          if (date != null) {
                            selectedType = "month";
                            setState(() => selectedDate =
                                "${date.year}-${date.month < 10 ? '0' + date.month.toString() : date.month}");
                            await fetchChart();
                          }
                        });
                      },
                      child: Text("รายเดือน",
                          style: Config.defaultTextStyle(color: Colors.white)),
                    ),
                    Text("เวลาที่เลือก: $selectedDate"),
                    Divider(),
                    Flex(
                      direction: Axis.horizontal,
                      children: [
                        Flexible(
                          flex: 1,
                          child: Text("การแสดงผลต่อหน้า"),
                        ),
                        Flexible(
                          flex: 5,
                          child: TextField(
                            controller: SlidingViewport.showLength,
                            keyboardType: TextInputType.number,
                            decoration:
                                InputDecoration(hintText: "การแสดงผลต่อหน้า"),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              )),
              Card(
                elevation: 10,
                child: Container(
                  margin: EdgeInsets.all(10),
                  height: 350,
                  width: MediaQuery.of(context).size.width,
                  child: isLoad ? LoadingScreen() : SubscriberChart(),
                ),
              ),
              Card(
                elevation: 10,
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Text(
                          "ค่าเฉลีย",
                          style: Config.defaultTextStyle(fontSize: 16),
                        ),
                      ),
                      Divider(),
                      Flex(
                        direction: Axis.horizontal,
                        children: [
                          Flexible(
                            flex: 6,
                            child: LinearPercentIndicator(
                              center: Text("${percent * 100}%"),
                              lineHeight: 15.0,
                              percent: percent,
                              progressColor: Theme.of(context).primaryColor,
                            ),
                          ),
                          Flexible(
                            flex: 4,
                            child: Text(
                              " ค่าเฉลี่ยที่ได้ $avg (ประสิทธิภาพ ${percent * 100} %)",
                              textAlign: TextAlign.end,
                              style: Config.defaultTextStyle(fontSize: 14),
                            ),
                          )
                        ],
                      ),
                      Divider(),
                      Center(
                        child: Text(
                          " ค่าที่แนะนำอยู่ที่ ${selected.min} ถึง ${selected.max} (ประสิทธิภาพ 100 %)",
                          textAlign: TextAlign.end,
                          style: Config.defaultTextStyle(fontSize: 14),
                        ),
                      ),
                      Divider(),
                      Table(
                        border: TableBorder.all(width: 0.3),
                        children: <TableRow>[
                              TableRow(children: [
                                Center(child: Text("เวลา")),
                                Center(child: Text("ค่าเฉลี่ย"))
                              ])
                            ] +
                            History.historyData
                                .map<TableRow>((e) => TableRow(children: [
                                      Center(child: Text("${e.label}")),
                                      Center(child: Text("${e.value}"))
                                    ]))
                                .toList(),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  fetchChart() async {
    setState(() => isLoad = true);
    try {
      header = selected.name;
      await History.getChart(selected.id,
          condition: "created_at like '$selectedDate%'", type: selectedType);
      avg = History.average;
      if (selected.id != null) {
        if (selected.min != 0 && selected.min > avg) {
          percent = (selected.min - avg) / selected.min;
        } else if (selected.max != 0 && selected.max < avg) {
          percent = (avg - selected.max) / selected.max % 1;
        } else {
          percent = 0.0;
        }
      }
    } catch (e) {
      Fluttertoast.showToast(msg: e.toString());
      print(e);
    }
    setState(() => isLoad = false);
  }
}

class History {
  final String label;
  final double value;
  final charts.Color barColor;
  static List<History> historyData = [];
  static double average;

  History(
      {@required this.label, @required this.value, @required this.barColor});

  static getChart(int deviceId,
      {@required String condition, @required String type}) async {
    average = 0;
    historyData.clear();
    String token = await Config.getToken();
    Map data = {"token": token};
    if (condition != null) {
      data.addAll({"condition": condition, "selectedType": type});
    }
    var res = await Config.myPost(
        data, Config.api['get-chart'] + deviceId.toString());

    if (res['status']) {
      for (var history in res['data']) {
        historyData.add(History(
            label: type == 'date'
                ? "เวลา " +
                    history['created_at'].toString().substring(11, 13) +
                    " น."
                : "วันที่ " + history['created_at'].toString().substring(8, 10),
            value: double.parse(history['value'].toString()),
            barColor: charts.ColorUtil.fromDartColor(Colors.amber[600])));
        average += double.parse(history['value'].toString());
      }
      average /= historyData.length;
    }
  }
}

class SubscriberChart extends StatefulWidget {
  @override
  _SubscriberChartState createState() => _SubscriberChartState();
}

class _SubscriberChartState extends State<SubscriberChart> {
  @override
  Widget build(BuildContext context) {
    List<charts.Series<History, String>> series = [
      charts.Series(
          id: "Subscribers",
          data: History.historyData,
          domainFn: (History series, _) => series.label,
          measureFn: (History series, _) => series.value,
          colorFn: (History series, _) => series.barColor)
    ];

    return charts.BarChart(
      series,
      animate: true,
      behaviors: [
        charts.SlidingViewport(),
        charts.PanAndZoomBehavior(),
      ],
      selectionModels: [
        charts.SelectionModelConfig(
            changedListener: (charts.SelectionModel model) {
          if (model.hasDatumSelection)
            print(model.selectedSeries[0]
                .measureFn(model.selectedDatum[0].index));
          Fluttertoast.showToast(
            msg: model.selectedSeries[0]
                .measureFn(model.selectedDatum[0].index)
                .toString(),
          );
        })
      ],
      domainAxis: charts.OrdinalAxisSpec(
          viewport: charts.OrdinalViewport(
              '0', int.parse(SlidingViewport.showLength.text))),
    );
  }
}
