import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mysmartfarm/models/Durian.dart';
import 'package:mysmartfarm/models/User.dart';
import 'package:mysmartfarm/screens/DurianDetailsPage.dart';
import 'package:mysmartfarm/screens/Loading.dart';

import '../Config.dart';

// ignore: must_be_immutable
class FormDurianDetialPage extends StatefulWidget {
  Durian durian;
  DurianDetails durianDetails;
  FormDurianDetialPage({this.durian, this.durianDetails});
  @override
  _FormDurianDetialPageState createState() => _FormDurianDetialPageState();
}

class _FormDurianDetialPageState extends State<FormDurianDetialPage> {
  DurianDetails durianDetails = DurianDetails();
  TextEditingController title = TextEditingController();
  TextEditingController content = TextEditingController();
  bool isLoad = false;
  File image;
  @override
  void initState() {
    super.initState();
    if (widget.durianDetails != null) {
      durianDetails = widget.durianDetails;
      title.text = durianDetails.title;
      content.text = durianDetails.content;
    }
    durianDetails.durianId = widget.durian.id;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "${widget.durianDetails.title}",
            style: TextStyle(color: Colors.white, fontFamily: "sarabun"),
          ),
          iconTheme: IconThemeData(color: Colors.white),
          centerTitle: true,
        ),
        body: isLoad
            ? LoadingScreen()
            : Center(
                child: Container(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Card(
                            child: MaterialButton(
                              onPressed: () async {
                                File oldImage = durianDetails.image;
                                try {
                                  // ignore: deprecated_member_use
                                  durianDetails.image =
                                      await ImagePicker.pickImage(
                                          source: ImageSource.gallery);
                                  setState(() => durianDetails.image);
                                } catch (e) {
                                  setState(
                                      () => durianDetails.image = oldImage);
                                }
                              },
                              height: 250,
                              child: Center(
                                child: (() {
                                  if (durianDetails.image != null) {
                                    return Image.file(durianDetails.image);
                                  } else if (durianDetails.imageUrl != null) {
                                    return Image.network(
                                        durianDetails.imageUrl);
                                  } else {
                                    return Wrap(children: [
                                      Icon(
                                        FontAwesomeIcons.fileImage,
                                        color: Colors.black,
                                      ),
                                      Text(
                                        " เลือกรูปภาพ",
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontFamily: "sarabun"),
                                      )
                                    ]);
                                  }
                                }()),
                              ),
                            ),
                          ),
                          Divider(),
                          TextField(
                            controller: title,
                            maxLength: 500,
                            onChanged: (value) =>
                                setState(() => durianDetails.title = value),
                            decoration: InputDecoration(
                                icon: Text("หัวข้อ"),
                                hintText: "เช่น บันทึกวันที่ 2"),
                          ),
                          Divider(),
                          TextField(
                            controller: content,
                            maxLength: 1000,
                            maxLines: 3,
                            onChanged: (value) =>
                                setState(() => durianDetails.content = value),
                            decoration: InputDecoration(
                                icon: Text("รายละเอียด"),
                                hintText:
                                    "เช่น ต้นทุเรียนมีขนาดที่ใหญ่ขึ้น 5 ซม."),
                          ),
                          Divider(),
                          MaterialButton(
                            minWidth: double.infinity,
                            color: Theme.of(context).buttonColor,
                            onPressed: () async {
                              DatePicker.showDatePicker(context,
                                  showTitleActions: true,
                                  onChanged: (date) {}, onConfirm: (date) {
                                setState(() => durianDetails.date =
                                    "${date.year}-${date.month}-${date.day}");
                              },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.th);
                            },
                            child: Text(
                              "${durianDetails.date ?? 'กรุณาเลือกวันที่'}",
                              style:
                                  Config.defaultTextStyle(color: Colors.white),
                            ),
                          ),
                          !(User.localUser.isAdmin == 1 ||
                                  User.localUser.isManager == 1 ||
                                  User.localUser.isOwner == 1)
                              ? Container()
                              : MaterialButton(
                                  minWidth: double.infinity,
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () async {
                                    if (!(User.localUser.isAdmin == 1 ||
                                        User.localUser.isManager == 1 ||
                                        User.localUser.isOwner == 1)) {
                                      return Fluttertoast.showToast(
                                          msg:
                                              "คุณไม่มีสิทธิ์ใช้งานฟังก์ชั่นนี้");
                                    }
                                    if ((durianDetails.image == null &&
                                            durianDetails.imageUrl == null) ||
                                        durianDetails.content == null ||
                                        durianDetails.title == null ||
                                        durianDetails.date == null ||
                                        durianDetails.durianId == null) {
                                      return Fluttertoast.showToast(
                                          msg:
                                              "กรุณากรอกข้อมูลให้ครบก่อนกดบันทึก");
                                    }
                                    setState(() => isLoad = true);
                                    try {
                                      bool saved = await durianDetails.save(
                                          isUpdate: widget.durianDetails == null
                                              ? false
                                              : true);
                                      if (saved) {
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                        Navigator.of(context).push(
                                            MaterialPageRoute(
                                                builder:
                                                    (BuildContext context) =>
                                                        DurianDetailsPage(
                                                          durian: widget.durian,
                                                        )));
                                        Fluttertoast.showToast(
                                            msg: "บันทึกสำเร็จ");
                                      } else {
                                        Fluttertoast.showToast(
                                            msg: "บันทึกไม่สำเร็จ");
                                      }
                                    } catch (e) {
                                      Config.buildShowDialog(
                                          context: context, message: "$e");
                                    }
                                    setState(() => isLoad = false);
                                  },
                                  child: Wrap(children: [
                                    Icon(
                                      FontAwesomeIcons.save,
                                      color: Colors.white,
                                    ),
                                    Text(
                                      " บันทึก",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "sarabun"),
                                    )
                                  ]),
                                )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
