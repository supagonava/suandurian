import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Auth.dart';
import 'package:mysmartfarm/models/Auth.dart';
import 'package:mysmartfarm/screens/ProfileUpdate.dart';
import 'package:mysmartfarm/widgets/ConfirmDialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'LoginPage.dart';

class SettingPage extends StatefulWidget {
  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 3),
            child: ListTile(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => UpdateProfile()));
              },
              leading: Icon(FontAwesomeIcons.userAlt),
              title: Text("โปรไฟล์ของฉัน",
                  style: Theme.of(context).textTheme.bodyText1),
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 3),
            child: ListTile(
              onTap: () async {
                Auth.context = context;
                bool confirm = await showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: ((BuildContext context) {
                      return ConfirmDialog(context);
                    }));
                if (confirm) {
                  await Auth.signOutFromSystem();
                }
              },
              leading: Icon(FontAwesomeIcons.doorOpen),
              title: Text("ออกจากระบบ",
                  style: Theme.of(context).textTheme.bodyText1),
            ),
          ),
          Container(
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 3),
            child: ListTile(
              onTap: () => showAboutDialog(
                  context: context,
                  applicationName: "My SmartFarm Platform",
                  applicationVersion: "1.0.0",
                  applicationIcon: Image.asset(
                    "assets/images/logo.png",
                    width: 50,
                    height: 50,
                  ),
                  applicationLegalese:
                      "แพลตฟอร์มบริหารจัดการฟาร์มอัจฉริยะทั่วไป\n(Universal Smart Farm Management Platform)"),
              leading: Icon(FontAwesomeIcons.info),
              title: Text("เกี่ยวกับ",
                  style: Theme.of(context).textTheme.bodyText1),
            ),
          ),
        ],
      ),
    );
  }
}
