import 'package:bottom_navigation_badge/bottom_navigation_badge.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/Notification.dart';
import 'package:mysmartfarm/screens/NewsPage.dart';
import 'package:mysmartfarm/screens/NotifiPage.dart';
import 'package:mysmartfarm/screens/SettingPage.dart';
import 'package:mysmartfarm/widgets/ConfirmDialog.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'HomePage.dart';
import "MapPage.dart";

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  //---------------------------------------------Local variable
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  int status = 0;
  List listFarm = List();
  bool enabled = true;
  bool isLoad = false;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  bool badeSupport = false;

  String message;
  String channelId = "1000";
  String channelName = "FLUTTER_NOTIFICATION_CHANNEL";
  String channelDescription = "FLUTTER_NOTIFICATION_CHANNEL_DETAIL";
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  sendNotification({title, body}) async {
    var bigTextStyleInformation = BigTextStyleInformation('$body',
        htmlFormatBigText: true,
        contentTitle: '$title',
        htmlFormatContentTitle: true,
        summaryText: 'การแจ้งเตือน',
        htmlFormatSummaryText: true);

    var androidPlatformChannelSpecifics = AndroidNotificationDetails('10000',
        'FLUTTER_NOTIFICATION_CHANNEL', 'FLUTTER_NOTIFICATION_CHANNEL_DETAIL',
        importance: Importance.max,
        priority: Priority.high,
        styleInformation: bigTextStyleInformation);

    var iOSPlatformChannelSpecifics = IOSNotificationDetails();

    var platformChannelSpecifics = NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
        0, '$title', '$body', platformChannelSpecifics);
  }

  initPlatformState() async {
    try {
      badeSupport = await FlutterAppBadger.isAppBadgeSupported();
    } on PlatformException {}

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
  }

  Future<void> initFirebaseMessaging() async {
    firebaseMessaging.subscribeToTopic('global');
    firebaseMessaging.configure(
      onMessage: (message) async {
        print("onMessage: $message");
        if (message['data']['farm_id'] != null &&
            Farm.farmList
                    .where((element) =>
                        element.id == int.parse(message['data']['farm_id']))
                    .length >
                0) {
          sendNotification(
              title: message['notification']['title'],
              body: message['notification']['body']);
        }

        if (badeSupport) {
          setState(() => _addBadge(MyNotification.notiList
              .where((element) => !element.read)
              .length));
        }
      },
      onLaunch: (message) async {
        print("onLaunch: $message");
        if (message['data']['farm_id'] != null &&
            Farm.farmList
                    .where((element) =>
                        element.id == int.parse(message['data']['farm_id']))
                    .length >
                0) {
          sendNotification(
              title: message['notification']['title'],
              body: message['notification']['body']);
        }
      },
      onResume: (message) async {
        print("onResume: $message");
        if (message['data']['farm_id'] != null &&
            Farm.farmList
                    .where((element) =>
                        element.id == int.parse(message['data']['farm_id']))
                    .length >
                0) {
          sendNotification(
              title: message['notification']['title'],
              body: message['notification']['body']);
        }
      },
    );
    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    var initializationSettingsAndroid =
        AndroidInitializationSettings('launcher_icon');
    var initializationSettingsIOS = IOSInitializationSettings(
        // ignore: missing_return
        onDidReceiveLocalNotification: (id, title, body, payload) {
      print("onDidReceiveLocalNotification called.");
    });

    var initializationSettings = InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        // ignore: missing_return
        onSelectNotification: (payload) {
      // when user tap on notification.
      print("onSelectNotification called.");
      setState(() {
        message = payload;
      });
    });
  }

  void showInfoFlushbar(BuildContext context, String text) {
    Flushbar(
      title: 'แจ้งเตือน',
      message: text,
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Color(0xff75C46B),
      ),
      leftBarIndicatorColor: Color(0xff75C46B),
      duration: Duration(seconds: 3),
    )..show(context);
  }

  List<BottomNavigationBarItem> items = [
    BottomNavigationBarItem(
        icon: Icon(Icons.home, color: Colors.black54),
        title: Text(
          "หน้าหลัก",
          style: TextStyle(color: Colors.black54),
        )),
    BottomNavigationBarItem(
        icon: Icon(Icons.notifications, color: Colors.black54),
        title: Text("การแจ้งเตือน", style: TextStyle(color: Colors.black54))),
    BottomNavigationBarItem(
        icon: FaIcon(FontAwesomeIcons.newspaper, color: Colors.black54),
        title: Text("ข่าว", style: TextStyle(color: Colors.black54))),
    BottomNavigationBarItem(
        icon: Icon(Icons.map, color: Colors.black54),
        title: Text("แผนที่", style: TextStyle(color: Colors.black54))),
    BottomNavigationBarItem(
        icon: Icon(Icons.settings, color: Colors.black54),
        title: Text("ตั้งค่า", style: TextStyle(color: Colors.black54)))
  ];
  void initState() {
    super.initState();
    myInitState();
  }

  void _addBadge(badgeSize) {
    if (badgeSize == 0) {
      _removeBadge();
    } else {
      FlutterAppBadger.updateBadgeCount(badgeSize);
    }
  }

  void _removeBadge() {
    FlutterAppBadger.removeBadge();
  }

  myInitState() async {
    setState(() => isLoad = true);
    await initFirebaseMessaging();
    await MyNotification.getNotifications();
    await initPlatformState();
    if (MyNotification.notiList.where((element) => !element.read).length > 0) {
      setState(() => items = badger.setBadge(
          items,
          MyNotification.notiList.where((element) => !element.read).length > 9
              ? "9+"
              : MyNotification.notiList
                  .where((element) => !element.read)
                  .length
                  .toString(),
          1));
      if (badeSupport) {
        setState(() => _addBadge(
            MyNotification.notiList.where((element) => !element.read).length));
      }
    }
    setState(() => isLoad = false);
  }

  BottomNavigationBadge badger = new BottomNavigationBadge(
      backgroundColor: Colors.red,
      badgeShape: BottomNavigationBadgeShape.circle,
      textColor: Colors.white,
      position: BottomNavigationBadgePosition.topRight,
      textSize: 8);
  int _selectedIndex = 0;

  Future<void> _onItemTapped(int index) async {
    if (index != 3) {
      setState(() {
        _selectedIndex = index;
      });
    }

    switch (index) {
      case 1:
        await myInitState();
        break;
      case 3:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) => Map()));
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // floatingActionButton: MaterialButton(
        //   colorBrightness: Brightness.dark,
        //   shape: CircleBorder(),
        //   onPressed: () => setState(() => _addBadge(2)),
        //   child: Icon(FontAwesomeIcons.bell),
        // ),
        appBar: mainAppBar(),
        bottomNavigationBar: BottomNavigationBar(
          items: items,
          currentIndex: _selectedIndex,
          onTap: _onItemTapped,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: IndexedStack(
                index: _selectedIndex,
                children: [
                  MyHome(),
                  NotifiPage(),
                  NewsPage(),
                  SizedBox(),
                  SettingPage()
                ],
              ),
            )));
  }

  AppBar mainAppBar() {
    var appBarTxt = [
      "หน้าหลัก",
      "การแจ้งเตือน",
      "ข่าวสาร",
      "แผนที่",
      "ตั้งค่า"
    ];
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xff72cb49),
      actions: [
        (() {
          if (_selectedIndex == 1) {
            return FlatButton(
                child: Text('อ่านทั้งหมด',
                    style:
                        TextStyle(color: Colors.white, fontFamily: "Sarabun")),
                onPressed: () async {
                  bool confirm = await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ConfirmDialog(context);
                      });
                  if (confirm != null && confirm) {
                    setState(() => NotifiPage.isLoad = true);
                    try {
                      for (var noti in MyNotification.notiList) {
                        noti.read = true;
                        await noti.readNoti();
                      }
                    } catch (e) {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                                title: Text("เกิดข้อผิดพลาด"),
                                content: Text("$e"));
                          });
                    }
                    setState(() => NotifiPage.isLoad = false);
                  }
                });
          } else {
            return Container();
          }
        }())
      ],
      title: Text(
        appBarTxt[_selectedIndex],
        style:
            TextStyle(fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
      ),
    );
  }
}
