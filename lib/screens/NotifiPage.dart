import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/Notification.dart';
import 'package:mysmartfarm/screens/DashBoardPage.dart';
import 'package:mysmartfarm/screens/Loading.dart';

class NotifiPage extends StatefulWidget {
  static bool isLoad = false;
  const NotifiPage({
    Key key,
  }) : super(key: key);

  @override
  _NotifiPageState createState() => _NotifiPageState();
}

class _NotifiPageState extends State<NotifiPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var notifications = MyNotification.notiList
        .map<Widget>((e) => Card(
              elevation: 5,
              child: ListTile(
                  onTap: () {
                    if (e.farmId != null) {
                      setState(() => e.read = true);
                      try {
                        e.readNoti();
                      } catch (e) {
                        Fluttertoast.showToast(msg: "$e");
                      }
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => DashboardPage(Farm
                              .farmList
                              .firstWhere((farm) => farm.id == e.farmId))));
                    }
                  },
                  leading: CircleAvatar(
                      backgroundColor: Theme.of(context).buttonColor,
                      child:
                          FaIcon(FontAwesomeIcons.bell, color: Colors.white)),
                  title: Text(e.title,
                      style: TextStyle(color: Colors.black, fontSize: 20)),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Wrap(
                      direction: Axis.vertical,
                      children: [
                        Text(
                          e.message,
                          style: TextStyle(color: Colors.black, fontSize: 16),
                        ),
                        Divider(),
                        Text(
                          e.createdAt,
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                  trailing: Wrap(
                    direction: Axis.horizontal,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Checkbox(
                        onChanged: (value) {
                          setState(() {
                            e.read = value;
                          });
                          try {
                            e.readNoti();
                          } catch (e) {
                            Fluttertoast.showToast(msg: "$e");
                          }
                        },
                        value: e.read,
                      ),
                      Text("${e.read ? 'อ่านแล้ว' : 'ยังไม่อ่าน'}"),
                    ],
                  )),
            ))
        .toList();

    if (NotifiPage.isLoad) {
      return LoadingScreen();
    } else {
      return MyNotification.notiList != null
          ? SingleChildScrollView(
              child: Column(
                  children: notifications +
                      [
                        MyNotification.nextUrl != null
                            ? MaterialButton(
                                minWidth: MediaQuery.of(context).size.width,
                                onPressed: () async {
                                  await MyNotification.getMoreNotifications();
                                  setState(() => MyNotification.notiList);
                                },
                                child: Text("คลิกเพื่อโหลดเพิ่มเติม..."),
                              )
                            : Container()
                      ]))
          : Center(
              child: Text("ไม่มีการแจ้งเตือนในขณะนี้"),
            );
    }
  }
}
