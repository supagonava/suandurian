import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:mysmartfarm/models/Auth.dart';
import 'LoginPage.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();
    checkLogin();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new InkWell(
        child: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                new Expanded(
                  flex: 2,
                  child: new Container(
                      child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new CircleAvatar(
                        backgroundColor: Theme.of(context).primaryColor,
                        child: new Container(
                            child: Image.asset('assets/images/logo.png')),
                        radius: 150,
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                      ),
                      SpinKitDoubleBounce(
                          color: Theme.of(context).primaryColor),
                      Text("ยินดีต้อนรับ",
                          style: TextStyle(
                              color: Colors.black54,
                              fontFamily: "Sarabun",
                              fontSize: 20))
                    ],
                  )),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  checkLogin() async {
    Auth.context = context;
    bool logedOn = await Auth.validateToken();
    if (!logedOn) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
          (route) => false);
    }
  }
}
