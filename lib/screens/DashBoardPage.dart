import 'package:animations/animations.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Auth.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/Iot.dart';
import 'package:mysmartfarm/models/User.dart';
import 'package:mysmartfarm/screens/DurianRecordPage.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import 'package:mysmartfarm/screens/UserManagePage.dart';
import '../Config.dart';
import '../screens/SettingFarm.dart';
import 'IotManagePage.dart';
import 'SlidingViewport.dart';

// ignore: must_be_immutable
class DashboardPage extends StatefulWidget {
  Farm farm;
  DashboardPage(this.farm);
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  var refreshKey = GlobalKey<RefreshIndicatorState>();
  List<IOT> _iots;
  bool isLoad = true;
  final FirebaseDatabase database = FirebaseDatabase();

  bool isViewer = false, isManager = false;
  void initState() {
    super.initState();
    myInitState();
  }

  myInitState() async {
    setState(() {
      isLoad = true;
    });
    await setIot();
    await widget.farm.getUser();
    for (var device in IOT.iotList) {
      database
          .reference()
          .child("farm/${widget.farm.id}/${device.qrcode}")
          .once()
          .then((DataSnapshot snapshot) => setState(
              () => device.value = double.parse(snapshot.value.toString())));
      database
          .reference()
          .child("farm/${widget.farm.id}/${device.qrcode}")
          .onValue
          .listen((Event event) {
        print("snapshot:" + event.snapshot.value.toString());
        setState(
            () => device.value = double.parse(event.snapshot.value.toString()));
      });
    }
    User.localUser =
        User.userList.firstWhere((element) => element.id == Auth.id);
    isViewer = User.localUser.isViewer == 0 ? false : true;
    isManager = User.localUser.isManager == 0 ? false : true;
    setState(() {
      _iots = IOT.iotList;
      isLoad = false;
    });
  }

  setIot() async {
    await IOT.getByFarm(widget.farm.id);
  }

  @override
  void dispose() {
    super.dispose();
    super.deactivate();
  }

  void destroyState() {
    super.dispose();
  }

  //------------------------------------------------
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: dashboardAppBar(),
        floatingActionButton: MaterialButton(
          height: 50,
          onPressed: () => myInitState(),
          child: Icon(FontAwesomeIcons.redoAlt),
          shape: CircleBorder(),
          colorBrightness: Brightness.light,
          padding: EdgeInsets.all(5),
          color: Colors.green[300],
        ),
        body: isLoad
            ? LoadingScreen()
            : GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                },
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                          padding: EdgeInsets.all(15),
                          margin:
                              EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("ชื่อ",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                  Text("${widget.farm.name}",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18))
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text("จำนวนแปลง",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18)),
                                  Text("${widget.farm.size}",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 18))
                                ],
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                              color: Theme.of(context).accentColor,
                              // image: DecorationImage(
                              //     colorFilter: ColorFilter.mode(
                              //         Colors.black, BlendMode.softLight),
                              //     fit: BoxFit.cover,
                              //     image:
                              //         AssetImage("assets/images/headerbg.jpg")),
                              borderRadius: BorderRadius.circular(10))),
                      Wrap(
                        direction: Axis.horizontal,
                        children: [
                          Card(
                              color: Theme.of(context).primaryColor,
                              shadowColor: Colors.black,
                              child: Container(
                                height: 75,
                                width: MediaQuery.of(context).size.width * 0.45,
                                padding: EdgeInsets.all(15),
                                child: Flex(
                                  children: [
                                    Flexible(
                                        flex: 1,
                                        child: Icon(Icons.device_hub,
                                            color: Colors.white)),
                                    Flexible(
                                      flex: 5,
                                      child: Padding(
                                        padding: const EdgeInsets.only(left: 4),
                                        child: Text(
                                          "อุปกรณ์ตรวจวัด ${IOT.iotList.length}",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontFamily: "Sarabun",
                                              color: Colors.white),
                                        ),
                                      ),
                                    )
                                  ],
                                  direction: Axis.horizontal,
                                ),
                              )),
                          Card(
                              color: Theme.of(context).buttonColor,
                              shadowColor: Colors.black,
                              child: Container(
                                height: 75,
                                width: MediaQuery.of(context).size.width * 0.45,
                                padding: EdgeInsets.all(15),
                                child: Flex(
                                  children: [
                                    Flexible(
                                        flex: 1,
                                        child: Icon(FontAwesomeIcons.userAlt,
                                            color: Colors.white)),
                                    Flexible(
                                      flex: 5,
                                      child: InkWell(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return AlertDialog(
                                                  title:
                                                      Text("รายชื่อผู้ใช้งาน"),
                                                  content:
                                                      SingleChildScrollView(
                                                    child: Column(
                                                      children: User.userList
                                                          .map((e) => ListTile(
                                                                title: Text("ชื่อ" +
                                                                        e.fullName ??
                                                                    e.email ??
                                                                    e.phone),
                                                                subtitle: Text(e
                                                                            .isViewer ==
                                                                        1
                                                                    ? 'ผู้สังเกตุการณ์'
                                                                    : e.isManager ==
                                                                            1
                                                                        ? 'ผู้จัดการฟาร์ม'
                                                                        : 'เจ้าของฟาร์ม'),
                                                              ))
                                                          .toList(),
                                                    ),
                                                  ),
                                                );
                                              });
                                        },
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 4),
                                          child: Text(
                                            "จำนวนผู้ใช้งาน ${User.userList.length}",
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontFamily: "Sarabun",
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                  direction: Axis.horizontal,
                                ),
                              )),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Card(
                          color: Colors.amber,
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "บทบาทของคุณคือ ${isManager ? 'ผู้จัดการ' : isViewer ? 'ผู้สังเกตุการณ์' : 'เจ้าของฟาร์ม'}",
                                style: Config.defaultTextStyle(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Card(
                          elevation: 3,
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.all(20.0),
                                        child: Text(
                                          "เซ็นเซอร์ที่เปิดการทำงาน",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontFamily: "Sarabun",
                                              fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  ] +
                                  IOT.iotList
                                      .where(
                                          (element) => element.status == "on")
                                      .map<Widget>((device) => Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          margin: EdgeInsets.symmetric(
                                              horizontal: 15, vertical: 5),
                                          padding: EdgeInsets.all(20),
                                          child: Column(
                                            children: [
                                              Text(
                                                  "${device.name} (${device.status == 'on' ? 'เปิด' : 'ปิด'})",
                                                  style: TextStyle(
                                                      fontSize: 25,
                                                      color: Colors.white,
                                                      fontFamily: "Sarabun")),
                                              Text(
                                                  "${device.value} ${device.unit}",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 40))
                                            ],
                                          ),
                                          decoration: BoxDecoration(
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black,
                                                    blurRadius: 3,
                                                    spreadRadius: 0.7)
                                              ],
                                              image: DecorationImage(
                                                  image: AssetImage((() {
                                                    device.variable = device.variable.toString();
                                                    if (device.variable
                                                        .contains("ดิน")) {
                                                      return "assets/images/soil.jpg";
                                                    } else if (device.variable
                                                        .contains("ชื้น")) {
                                                      return "assets/images/moisture.jpg";
                                                    } else if (device.variable
                                                        .contains("อุณหภูมิ")) {
                                                      return "assets/images/thermometer.jpg";
                                                    } else if (device.variable
                                                        .contains("ออกซิเจน")) {
                                                      return "assets/images/oxygen.jpg";
                                                    } else if (device.variable
                                                        .contains("แสง")) {
                                                      return "assets/images/light.jpg";
                                                    } else {
                                                      return "assets/images/nature.jpg";
                                                    }
                                                  }())),
                                                  fit: BoxFit.cover,
                                                  colorFilter: ColorFilter.mode(
                                                      Colors.black26,
                                                      BlendMode.darken)),
                                              borderRadius:
                                                  BorderRadius.circular(25))))
                                      .toList()),
                        ),
                      ),
                      Wrap(
                        spacing: 5,
                        runSpacing: 5,
                        children: [
                          viewhistBtn(),
                          homeBtn(),
                        ],
                      ),
                      SizedBox(height: 15),
                      settingVariables(),
                      manageIot(),
                      manageUser(),
                      manageDurian()
                    ],
                  ),
                )));
  }

  homeBtn() {
    return MaterialButton(
      minWidth: MediaQuery.of(context).size.width * 0.4,
      height: 100,
      onPressed: () => Navigator.of(context).pop(),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(
          children: [
            Icon(FontAwesomeIcons.home, color: Colors.white),
            Text(" กลับหน้าหลัก",
                style: TextStyle(color: Colors.white, fontFamily: "sarabun"))
          ],
        ),
      ),
      color: Theme.of(context).primaryColor,
    );
  }

  viewhistBtn() {
    return MaterialButton(
      minWidth: MediaQuery.of(context).size.width * 0.4,
      height: 100,
      onPressed: () => Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => SlidingViewport())),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Wrap(
          children: [
            Icon(FontAwesomeIcons.history, color: Colors.white),
            Text(" ประวัติย้อนหลัง",
                style: TextStyle(color: Colors.white, fontFamily: "sarabun"))
          ],
        ),
      ),
      color: Theme.of(context).buttonColor,
    );
  }

  Container settingVariables() {
    if (User.localUser.isManager == 1 || User.localUser.isOwner == 1) {
      return Container(
        margin: EdgeInsets.only(left: 35, right: 35, bottom: 10),
        color: Colors.transparent,
        child: Container(
          height: 50,
          decoration: BoxDecoration(
            color: Color(0xff707070),
            border: Border.all(
              width: 2.00,
              color: Colors.transparent,
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(0.00, 3.00),
                color: Color(0xff000000).withOpacity(0.18),
                blurRadius: 6,
              ),
            ],
            borderRadius: BorderRadius.circular(9.00),
          ),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      SettingFarmPage(widget.farm, _iots)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.edit,
                  color: Colors.white,
                ),
                Text(
                  '  ตั้งค่าฟาร์ม',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Sarabun",
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Container();
  }

  Container manageIot() {
    if (User.localUser.isManager == 1 || User.localUser.isOwner == 1) {
      return Container(
        margin: EdgeInsets.only(left: 35, right: 35, bottom: 10),
        color: Colors.transparent,
        child: Container(
          height: 50,
          decoration: BoxDecoration(
            color: Color(0xff707070),
            border: Border.all(
              width: 2.00,
              color: Colors.transparent,
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(0.00, 3.00),
                color: Color(0xff000000).withOpacity(0.18),
                blurRadius: 6,
              ),
            ],
            borderRadius: BorderRadius.circular(9.00),
          ),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      IotManagementPage(widget.farm)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.tools,
                  color: Colors.white,
                ),
                Text(
                  '  จัดการอุปกรณ์',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Sarabun",
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Container();
  }

  Container manageUser() {
    if (User.localUser.isOwner == 1 || User.localUser.isOwner == 1) {
      return Container(
        margin: EdgeInsets.only(left: 35, right: 35, bottom: 10),
        color: Colors.transparent,
        child: Container(
          height: 50,
          decoration: BoxDecoration(
            color: Color(0xff707070),
            border: Border.all(
              width: 2.00,
              color: Colors.transparent,
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(0.00, 3.00),
                color: Color(0xff000000).withOpacity(0.18),
                blurRadius: 6,
              ),
            ],
            borderRadius: BorderRadius.circular(9.00),
          ),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      UserManagementPage(widget.farm)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.userEdit,
                  color: Colors.white,
                ),
                Text(
                  '  จัดการผู้ใช้งาน',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Sarabun",
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Container();
  }

  Container manageDurian() {
    if (User.localUser.isManager == 1 || User.localUser.isOwner == 1) {
      return Container(
        margin: EdgeInsets.only(left: 35, right: 35, bottom: 35),
        color: Colors.transparent,
        child: Container(
          height: 50,
          decoration: BoxDecoration(
            color: Color(0xff707070),
            border: Border.all(
              width: 2.00,
              color: Colors.transparent,
            ),
            boxShadow: [
              BoxShadow(
                offset: Offset(0.00, 3.00),
                color: Color(0xff000000).withOpacity(0.18),
                blurRadius: 6,
              ),
            ],
            borderRadius: BorderRadius.circular(9.00),
          ),
          child: FlatButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      DurianRecordPage(farm: widget.farm)));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.tree,
                  color: Colors.white,
                ),
                Text(
                  '  จัดการต้นทุเรียน',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: "Sarabun",
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    }
    return Container();
  }

  AppBar dashboardAppBar() {
    return AppBar(
      centerTitle: true,
      backgroundColor: Color(0xff72cb49),
      iconTheme: IconThemeData(color: Colors.white),
      title: Text(
        "${widget.farm.name}",
        style:
            TextStyle(fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
      ),
    );
  }
}
