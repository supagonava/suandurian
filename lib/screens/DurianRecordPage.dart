import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mysmartfarm/models/Durian.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/User.dart';
import 'package:mysmartfarm/screens/DurianDetailsPage.dart';
import 'package:mysmartfarm/screens/FormDurian.dart';
import 'package:mysmartfarm/widgets/ConfirmDialog.dart';

import '../Config.dart';
import 'Loading.dart';

// ignore: must_be_immutable
class DurianRecordPage extends StatefulWidget {
  Farm farm;
  DurianRecordPage({this.farm});
  @override
  _DurianRecordPageState createState() => _DurianRecordPageState();
}

class _DurianRecordPageState extends State<DurianRecordPage> {
  bool isLoad = false;

  @override
  void initState() {
    super.initState();
    myInitState();
  }

  Future<void> myInitState() async {
    setState(() => isLoad = true);
    await Durian.getDurians(widget.farm.id);
    setState(() => isLoad = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: ButtonTheme(
        height: 80,
        child: FlatButton.icon(
          color: Theme.of(context).buttonColor,
          onPressed: () {
            if (!(User.localUser.isAdmin == 1 ||
                User.localUser.isManager == 1 ||
                User.localUser.isOwner == 1)) {
              return Fluttertoast.showToast(
                  msg: "คุณไม่มีสิทธิ์ใช้งานฟังก์ชั่นนี้");
            }
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => FormDurian(
                      farm: widget.farm,
                    )));
          },
          icon: FaIcon(FontAwesomeIcons.plus, color: Colors.white),
          label: Text("เพิ่มต้นทุเรียน",
              style: TextStyle(
                  color: Colors.white, fontFamily: "Sarabun", fontSize: 20)),
        ),
      ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "จัดการต้นทุเรียน",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
      ),
      body: isLoad
          ? LoadingScreen()
          : SingleChildScrollView(
              child: Column(
                children: Durian.durianList.isEmpty
                    ? [
                        Center(
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.7,
                            child: Center(
                                child: Text("ไม่มีต้นทุเรียนที่บันทึก",
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 20,
                                        fontFamily: "Sarabun"))),
                          ),
                        )
                      ]
                    : Durian.durianList
                        .map<Widget>((e) => Card(
                              elevation: 1.5,
                              margin: EdgeInsets.symmetric(vertical: 2),
                              child: Column(
                                children: [
                                  ListTile(
                                    onTap: () => Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                DurianDetailsPage(
                                                  durian: e,
                                                ))),
                                    contentPadding: EdgeInsets.all(6),
                                    title: Text(e.name,
                                        style: Config.defaultTextStyle(
                                            fontSize: 20)),
                                    subtitle: Text("บันทึกเมือ ${e.createdAt}"),
                                    leading: CachedNetworkImage(
                                      imageUrl: e.imageUrl,
                                      fit: BoxFit.contain,
                                      alignment: Alignment.center,
                                      placeholder: (context, url) => Center(
                                          child: CircularProgressIndicator()),
                                      errorWidget: (context, url, error) =>
                                          Icon(Icons.error),
                                    ),
                                  ),
                                  User.localUser.isAdmin == 1 ||
                                          User.localUser.isOwner == 1 ||
                                          User.localUser.isManager == 1
                                      ? Wrap(
                                          spacing: 15,
                                          direction: Axis.horizontal,
                                          children: [
                                              MaterialButton(
                                                  onPressed: () => Navigator.of(
                                                          context)
                                                      .push(MaterialPageRoute(
                                                          builder: (BuildContext
                                                                  context) =>
                                                              FormDurian(
                                                                farm:
                                                                    widget.farm,
                                                                durian: e,
                                                              ))),
                                                  child: Icon(
                                                      FontAwesomeIcons.edit,
                                                      size: 16,
                                                      color: Colors.white),
                                                  color: Colors.blueAccent),
                                              MaterialButton(
                                                  onPressed: () async {
                                                    bool confirm =
                                                        await showDialog(
                                                            context: context,
                                                            builder:
                                                                (BuildContext
                                                                    context) {
                                                              return ConfirmDialog(
                                                                  context);
                                                            });
                                                    if (confirm != null &&
                                                        confirm) {
                                                      setState(
                                                          () => isLoad = true);
                                                      bool deleted =
                                                          await e.delete();
                                                      if (deleted) {
                                                        Fluttertoast.showToast(
                                                            msg: "ลบสำเร็จ!");
                                                        Navigator.of(context)
                                                            .pop();
                                                      } else {
                                                        Fluttertoast.showToast(
                                                            msg:
                                                                "ล้มเหลวในการลบ!");
                                                      }
                                                      setState(
                                                          () => isLoad = false);
                                                    }
                                                  },
                                                  child: Icon(
                                                      FontAwesomeIcons.trash,
                                                      size: 16,
                                                      color: Colors.white),
                                                  color: Colors.red),
                                            ])
                                      : Container()
                                ],
                              ),
                            ))
                        .toList(),
              ),
            ),
    );
  }
}
