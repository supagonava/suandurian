import 'dart:io';

import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import '../Config.dart';
import '../widgets/MyTextField.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/Auth.dart';

class LoginPage extends StatefulWidget {
  @override
  __LoginPageState createState() => __LoginPageState();
}

class __LoginPageState extends State<LoginPage> {
  bool isAccept = false;
  bool isLoad = true;
  SharedPreferences prefs;
  bool supportsAppleSignIn = false;

  MyTextField usernameTxtFld =
      MyTextField(txtFieldString: "ชื่อผู้ใช้", inputType: TextInputType.text);

  MyTextField passwordTxtFld = MyTextField(
    txtFieldString: "รหัสผ่าน",
    inputType: TextInputType.visiblePassword,
  );

  void initState() {
    super.initState();
    Auth.context = context;
    myAsyncInit();
  }

  myAsyncInit() async {
    await Auth.validateToken();
    if (Platform.isIOS) {
      supportsAppleSignIn = await AppleSignIn.isAvailable();
      setState(() => supportsAppleSignIn);
    }
    setState(() => isLoad = false);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: isLoad == false
              ? Center(
                  child: SingleChildScrollView(
                      child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          width: 200,
                          height: 200,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/images/logo.png"),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        SizedBox(height: 8),
                        Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          elevation: 5,
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.55,
                                      child: TextField(
                                        controller:
                                            Auth.phoneNumberTxtController,
                                        decoration: InputDecoration(
                                          filled: true,
                                          hintText: "เบอร์มือถือ 10 หลัก",
                                          fillColor: Colors.white,
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Theme.of(context)
                                                    .primaryColor),
                                            borderRadius:
                                                BorderRadius.circular(25.7),
                                          ),
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: Colors.black38),
                                            borderRadius:
                                                BorderRadius.circular(25.7),
                                          ),
                                        ),
                                        keyboardType: TextInputType.number,
                                        style: TextStyle(
                                          fontFamily: "Sarabun",
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 5),
                                      child: ButtonTheme(
                                        height: 50,
                                        child: FlatButton(
                                            color:
                                                Theme.of(context).primaryColor,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(25.7),
                                            ),
                                            child: Text(
                                              "ส่ง OTP",
                                              style: TextStyle(
                                                  fontFamily: "Sarabun",
                                                  color: Colors.white),
                                            ),
                                            onPressed: () async {
                                              try {
                                                await Auth.signIn(1);
                                              } catch (e) {
                                                Config.buildShowDialog(
                                                    context: context,
                                                    message: e);
                                              }
                                            }),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 15),
                                Divider(),
                                SignInButton(
                                  Buttons.Google,
                                  text: "เข้าสู่ระบบด้วยจีเมล์",
                                  onPressed: () async {
                                    try {
                                      await Auth.signIn(2);
                                    } catch (e) {
                                      Config.buildShowDialog(
                                          context: context, message: e);
                                    }
                                  },
                                ),
                                SignInButton(
                                  Buttons.FacebookNew,
                                  text: "เข้าสู่ระบบด้วยเฟสบุค",
                                  mini: false,
                                  onPressed: () async {
                                    try {
                                      await Auth.signIn(3);
                                    } catch (e) {
                                      Config.buildShowDialog(
                                          context: context, message: e);
                                    }
                                  },
                                ),
                                supportsAppleSignIn
                                    ? SignInButton(
                                        Buttons.AppleDark,
                                        text: "เข้าสู่ระบบด้วยแอปเปิ้ล",
                                        mini: false,
                                        onPressed: () async {
                                          try {
                                            await Auth.signIn(4);
                                          } catch (e) {
                                            Config.buildShowDialog(
                                                context: context, message: e);
                                          }
                                        },
                                      )
                                    : Container(),
                                MaterialButton(
                                  minWidth: 220,
                                  color: Theme.of(context).primaryColor,
                                  child: Text("LINE เข้าสู่ระบบด้วยไลน์",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "Sarabun")),
                                  onPressed: () async {
                                    try {
                                      await Auth.signIn(5);
                                    } catch (e) {
                                      Config.buildShowDialog(
                                          context: context, message: e);
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )),
                )
              : LoadingScreen()),
    );
  }
}
