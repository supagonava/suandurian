import 'dart:async';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mysmartfarm/models/Farm.dart';
import 'package:mysmartfarm/models/Iot.dart';
import 'package:mysmartfarm/models/User.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import 'package:mysmartfarm/widgets/ConfirmDialog.dart';
import 'package:qr_code_tools/qr_code_tools.dart';
import '../Config.dart';
import '../models/QRcode.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';

class SubmitUserPage extends StatefulWidget {
  final Farm farm;

  const SubmitUserPage({Key key, this.farm});

  @override
  _SubmitUserPageState createState() => _SubmitUserPageState();
}

class _SubmitUserPageState extends State<SubmitUserPage> {
  ScanResult scanResult;
  User user;
  bool isLoad = false;
  TextEditingController qrcode = TextEditingController();
  var status;
  final _flashOnController = TextEditingController(text: "เปิดแฟรช");
  final _flashOffController = TextEditingController(text: "ปิดแฟรช");
  final _cancelController = TextEditingController(text: "ยกเลิก");

  var _aspectTolerance = 0.5;
  var _selectedCamera = -1;
  var _useAutoFocus = true;
  var _autoEnableFlash = false;

  String qr, name, variable;
  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  List<BarcodeFormat> selectedFormats = _possibleFormats;

  @override
  void initState() {
    super.initState();
    QRCODE.scanResult = null;
  }

  findUser(qrc) async {
    setState(() {
      isLoad = true;
    });
    try {
      if (qrc.toString().isNotEmpty) {
        var token = await Config.getToken();
        Map data = {"token": token};
        var res = await Config.myPost(data, Config.api['get-user-qr'] + qrc);
        if (res['status']) {
          setState(() => user = User(
              id: res['data']['data']['id'],
              fullName: res['data']['data']['full_name'],
              qrcode: res['data']['data']['qrcode'],
              email: res['data']['data']['email'],
              isManager: 0,
              isViewer: 1,
              phone: res['data']['data']['phone']));
        } else {
          setState(() {
            user = null;
          });
          Fluttertoast.showToast(msg: "ไม่พบผู้ใช้ดังกล่าว");
        }
      } else {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                  title: Text("แจ้งเตือน"),
                  content: Text("ไม่สามารถอ่านเลข qr-code ได้"),
                  actions: [
                    RaisedButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text("ตกลง"),
                    ),
                  ]);
            });
      }
    } catch (e) {
      Fluttertoast.showToast(msg: "$e");
    }
    setState(() => isLoad = false);
  }

  Future decode(String file) async {
    String data = '';
    try {
      data = await QrCodeToolsPlugin.decodeFrom(file);
    } catch (e) {
      Fluttertoast.showToast(msg: "คิวอาร์โค้ดไม่ถูกต้อง");
    }
    findUser(data);
  }

  Future scan() async {
    status = await Permission.camera.status;
    if (!(status.toString().contains("PermissionStatus.granted"))) {
      await Permission.camera.request();
    } else {
      try {
        var options = ScanOptions(
          strings: {
            "cancel": _cancelController.text,
            "flash_on": _flashOnController.text,
            "flash_off": _flashOffController.text,
          },
          restrictFormat: selectedFormats,
          useCamera: _selectedCamera,
          autoEnableFlash: _autoEnableFlash,
          android: AndroidOptions(
            aspectTolerance: _aspectTolerance,
            useAutoFocus: _useAutoFocus,
          ),
        );

        var result = await BarcodeScanner.scan(options: options);
        QRCODE.scanResult = result.rawContent;
        setState(() => scanResult = result);
        await findUser(result.rawContent);
      } on PlatformException catch (e) {
        var result = ScanResult(
          type: ResultType.Error,
          format: BarcodeFormat.unknown,
        );

        if (e.code == BarcodeScanner.cameraAccessDenied) {
          setState(() {
            result.rawContent =
                'ไม่สามารถเข้าถึงกล้องได้ กรุณาเปิดสิทธิ์การเข้าถึงกล้อง!';
          });
        } else {
          result.rawContent = 'ข้อผิดพลาดที่ไม่รู้จัก: $e';
        }
        setState(() {
          scanResult = result;
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    var contentList = user != null
        ? <Widget>[
            Card(
              elevation: 10,
              child: Column(
                children: <Widget>[
                  ListTile(
                    tileColor: Colors.lightBlue,
                    title: Text("ข้อมูล",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sarabun",
                            fontSize: 16)),
                  ),
                  ListTile(
                    title: Text("เลข QR-CODE"),
                    subtitle: Text(user.qrcode ?? ""),
                  ),
                  ListTile(
                    title: Text("ชื่อ"),
                    subtitle: Text(user.fullName ?? "ไม่ระบุ"),
                  ),
                  ListTile(
                    title: Text("อีเมล์"),
                    subtitle: Text(user.email ?? "ไม่ระบุ"),
                  ),
                  ListTile(
                    title: Text("เบอร์"),
                    subtitle: Text(user.phone ?? "ไม่ระบุ"),
                  ),
                ],
              ),
            ),
            Flex(direction: Axis.horizontal, children: [
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                      color: Theme.of(context).primaryColor,
                      onPressed: () async {
                        try {
                          bool confirm = await showDialog(
                              context: context,
                              builder: (_) {
                                return ConfirmDialog(context);
                              });
                          if (confirm != null && confirm) {
                            setState(() {
                              isLoad = true;
                            });
                            bool isFinish =
                                await user.updateRole(widget.farm.id, 30);
                            if (isFinish) {
                              Fluttertoast.showToast(
                                  msg:
                                      "เพิ่มไปยังสวน ${widget.farm.name} สำเร็จ");
                              await IOT.getByFarm(widget.farm.id);
                              setState(() => IOT.iotList);
                              Navigator.of(context).pop();
                              Navigator.of(context).pop();
                            } else {
                              Fluttertoast.showToast(
                                  msg:
                                      "เพิ่มไปยังสวน ${widget.farm.name} ไม่สำเร็จ");
                            }
                            setState(() => isLoad = false);
                          }
                        } catch (e) {
                          Fluttertoast.showToast(msg: "$e");
                        }
                      },
                      icon: Icon(FontAwesomeIcons.check, color: Colors.white),
                      label: Text("เพิ่มผู้ใช้",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sarabun",
                            fontSize: 16,
                          ))),
                ),
              ),
              Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: FlatButton.icon(
                      color: Colors.redAccent,
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(FontAwesomeIcons.times, color: Colors.white),
                      label: Text("ยกเลิก",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sarabun",
                            fontSize: 16,
                          ))),
                ),
              ),
            ])
          ]
        : <Widget>[];

    return Scaffold(
      body: isLoad
          ? LoadingScreen()
          : GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: SingleChildScrollView(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(height: 30),
                  ButtonTheme(
                    height: 100,
                    minWidth: 100,
                    child: FlatButton(
                      onPressed: () async {
                        await scan();
                      },
                      child: Column(
                        children: [
                          Icon(FontAwesomeIcons.qrcode, size: 100),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("กดที่นี่เพื่อแสกน",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontFamily: "Sarabun",
                                    fontSize: 16)),
                          )
                        ],
                      ),
                    ),
                  ),
                  ButtonTheme(
                    height: 100,
                    minWidth: 100,
                    child: FlatButton(
                      onPressed: () async {
                        File file = await ImagePicker.pickImage(
                            source: ImageSource.gallery);
                        await decode(file.path);
                      },
                      child: Column(
                        children: [
                          Icon(FontAwesomeIcons.images, size: 100),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("เลือกไฟล์จากรูป",
                                style: TextStyle(
                                    color: Colors.black87,
                                    fontFamily: "Sarabun",
                                    fontSize: 16)),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 30.0, left: 30, right: 30, bottom: 5),
                    child: Flex(direction: Axis.horizontal, children: [
                      Flexible(flex: 1, child: Text("เลข QR-CODE:")),
                      Flexible(
                          flex: 3,
                          child: TextField(
                            controller: qrcode,
                          ))
                    ]),
                  ),
                  MaterialButton(
                    onPressed: () async {
                      await findUser(qrcode.text);
                    },
                    animationDuration: Duration(seconds: 1),
                    child: Icon(FontAwesomeIcons.search),
                    height: 50,
                    minWidth: MediaQuery.of(context).size.width * 0.95,
                    color: Theme.of(context).primaryColor,
                  ),
                  Divider(),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(children: contentList),
                  ),
                ],
              )),
            ),
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "เพิ่มผู้ใช้",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }
}
