import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Loading screen',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
      ),
      home: Scaffold(
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Positioned.fill(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SpinKitDoubleBounce(
                          color: Theme.of(context).primaryColor),
                      Text("กรุณารอสักครู่...",
                          style: TextStyle(
                              color: Colors.black54,
                              fontFamily: "Sarabun",
                              fontSize: 20))
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
