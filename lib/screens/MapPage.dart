import 'dart:async';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mysmartfarm/models/Farm.dart';

class Map extends StatefulWidget {
  const Map({
    Key key,
  }) : super(key: key);

  @override
  _MapState createState() => _MapState();
}

class _MapState extends State<Map> {
  Completer<GoogleMapController> _controller = Completer();
  bool showFab = true;
  Set<Marker> ewtcMarker() {
    return Farm.farmList
        .map((farm) => Marker(
            markerId: MarkerId('${farm.id}'),
            position: LatLng(farm.location.lat, farm.location.lng),
            infoWindow: InfoWindow(
                title: "${farm.name}", snippet: "ขนาด ${farm.size} แปลง")))
        .toList()
        .toSet();
  }

  Future _goToLatLng(double lat, double lng) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, lng), zoom: 15)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
        title: Text(
          "แผนที่สวนทุเรียน",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
      ),
      body: Flex(
        direction: Axis.vertical,
        children: [
          Flexible(
            flex: 10,
            child: GoogleMap(
              myLocationEnabled: true,
              mapType: MapType.hybrid,
              mapToolbarEnabled: true,
              buildingsEnabled: true,
              initialCameraPosition: CameraPosition(
                target: Farm.farmList.length > 0
                    ? LatLng(Farm.farmList[0].location.lat,
                        Farm.farmList[0].location.lng)
                    : LatLng(12.659338, 102.1335906),
                zoom: 12,
              ),
              markers: ewtcMarker(),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
          ),
          Flexible(
              flex: 1,
              child: ButtonTheme(
                height: double.infinity,
                minWidth: double.infinity,
                child: FlatButton.icon(
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      _settingModalBottomSheet(context);
                    },
                    icon: Icon(FontAwesomeIcons.listOl, color: Colors.white),
                    label: Text("กดดูฟาร์มของฉัน",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: "Sarabun",
                            fontSize: 18))),
              ))
        ],
      ),
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Material(
            child: Container(
              height: 300,
              child: ListView(
                scrollDirection: Axis.vertical,
                children: Farm.farmList
                    .map((e) => Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ButtonTheme(
                            height: 50,
                            child: RaisedButton(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25)),
                                color: Theme.of(context).buttonColor,
                                child: Row(
                                  children: [
                                    Icon(Icons.directions, color: Colors.white),
                                    Text(" ${e.name}",
                                        style: TextStyle(color: Colors.white)),
                                  ],
                                ),
                                onPressed: () => _goToLatLng(
                                    e.location.lat, e.location.lng)),
                          ),
                        ))
                    .toList(),
              ),
            ),
          );
        });
  }
}
