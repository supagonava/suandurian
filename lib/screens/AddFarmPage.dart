import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:place_picker/entities/localization_item.dart';
import 'package:toast/toast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '../models/Farm.dart';
import '../widgets/MyTextField.dart';
import 'Loading.dart';
import '../mylib/CustomPlacePicker.dart';
import 'MainPage.dart';

class AddFarmPage extends StatefulWidget {
  @override
  _AddFarmPageState createState() => _AddFarmPageState();
}

class _AddFarmPageState extends State<AddFarmPage> {
  FarmLatLng selectedPosition;
  bool isLoad = false;
  final MyTextField farmnameTxtFld = MyTextField(
      txtFieldString: "ชื่อสวนทุเรียน", inputType: TextInputType.text);
  final MyTextField sizeFarmTxtFld =
      MyTextField(txtFieldString: "จำนวนแปลง", inputType: TextInputType.number);
  void showInfoFlushbar(BuildContext context, String text) {
    Flushbar(
      title: 'แจ้งเตือน',
      message: text,
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Color(0xff75C46B),
      ),
      leftBarIndicatorColor: Color(0xff75C46B),
      duration: Duration(seconds: 3),
    )..show(context);
  }

  _createFarm(BuildContext context) async {
    setState(() {
      isLoad = true;
    });
    if (selectedPosition != null &&
        sizeFarmTxtFld.txtController.text.isNotEmpty &&
        farmnameTxtFld.txtController.text.isNotEmpty) {
      Farm farm = Farm(
          id: null,
          name: farmnameTxtFld.txtController.text,
          size: double.parse(sizeFarmTxtFld.txtController.text),
          location: selectedPosition);
      try {
        var isCreate = await farm.createFarm();
        print("isCreate:$isCreate");
        if (isCreate) {
          setState(() {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (BuildContext context) => MainPage()),
                (route) => false);
          });
        }
      } catch (e) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("แจ้งเตือน"),
                content: Text("หมายเหตุ:$e"),
                actions: <Widget>[
                  FlatButton(
                    child: Text("ตกลง"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
      }
    } else {
      Toast.show("กรุณากรอกข้อมูลให้ครบถ้วนก่อน", context,
          duration: 1, gravity: Toast.BOTTOM);
    }
    setState(() {
      isLoad = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "ลงทะเบียนสวนทุเรียน",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: !isLoad
          ? GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: Container(
                color: Color(0xffE8E8E8),
                child: Container(
                  padding: EdgeInsets.all(15),
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "ชื่อสวนทุเรียน",
                        style: TextStyle(fontFamily: "sarabun", fontSize: 16),
                      ),
                      farmnameTxtFld,
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "จำนวนแปลง",
                        style: TextStyle(fontFamily: "sarabun", fontSize: 16),
                      ),
                      sizeFarmTxtFld,
                      SizedBox(
                        height: 20,
                      ),
                      ButtonTheme(
                        height: 50,
                        child: FlatButton.icon(
                            onPressed: () {
                              showPlacePicker();
                            },
                            color: Theme.of(context).primaryColor,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15)),
                            icon: FaIcon(FontAwesomeIcons.searchLocation,
                                color: Colors.white),
                            label: Text(
                                selectedPosition == null
                                    ? "ระบุตำแหน่ง"
                                    : "ระบุตำแหน่งแล้ว",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Sarabun"))),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      confirmAddBtn(context),
                      SizedBox(
                        height: 10,
                      ),
                      cancelBtn(context),
                    ],
                  ),
                ),
              ),
            )
          : LoadingScreen(),
    );
  }

  Container confirmAddBtn(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Container(
        height: 50,
        width: 315,
        decoration: BoxDecoration(
          color: Color(0xff72cb49),
          border: Border.all(
            width: 2.00,
            color: Colors.transparent,
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0.00, 3.00),
              color: Color(0xff000000).withOpacity(0.16),
              blurRadius: 6,
            ),
          ],
          borderRadius: BorderRadius.circular(9.00),
        ),
        child: FlatButton(
          onPressed: () => _createFarm(context),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.check,
                color: Colors.white,
              ),
              Text(
                '  ยืนยัน',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: "Sarabun",
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Container cancelBtn(BuildContext context) {
    return Container(
      color: Colors.transparent,
      child: Container(
        height: 50,
        width: 315,
        decoration: BoxDecoration(
          color: Color(0xffff8a5b),
          border: Border.all(
            width: 2.00,
            color: Colors.transparent,
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0.00, 3.00),
              color: Color(0xff000000).withOpacity(0.16),
              blurRadius: 6,
            ),
          ],
          borderRadius: BorderRadius.circular(9.00),
        ),
        child: FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.cancel,
                color: Colors.white,
              ),
              Text(
                '  ยกเลิก',
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: "Sarabun",
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void showPlacePicker() async {
    Map<String, double> result =
        await Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => CustomPlacePicker(
                  "AIzaSyB5eiAIhp5Kg53iwEj4ytwP9-x28tyjzf4",
                  localizationItem: LocalizationItem(
                      findingPlace: 'ค้นหา',
                      languageCode: 'th_TH',
                      unnamedLocation: 'แตะเพื่อเลือกพิกัดนี้',
                      nearBy: 'ตำแหน่งใกล้เคียง',
                      noResultsFound: 'ไม่พบรายการ',
                      tapToSelectLocation: ''),
                )));

    // Handle the result in your way
    // print(result['lat']);
    try {
      setState(() {
        selectedPosition = FarmLatLng(lat: result['lat'], lng: result['lng']);
      });
    } catch (e) {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("แจ้งเตือน"),
              content: Text("ไม่ได้เลือกตำแหน่ง หมายเหตุ:$e"),
              actions: <Widget>[
                FlatButton(
                  child: Text("ตกลง"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }
}
