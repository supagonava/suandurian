import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysmartfarm/models/Auth.dart';
import 'package:qr_flutter/qr_flutter.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "โปรไฟล์ของฉัน",
          style: TextStyle(
              fontFamily: "Sarabun", fontSize: 20, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Color(0xff72cb49),
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                QrImage(
                  data: Auth.qrcode,
                  version: QrVersions.auto,
                  size: 200.0,
                ),
                SelectableText("QR-CODE: ${Auth.qrcode} \n(จับภาพหน้าจอเพื่อบันทึก)",
                    style: TextStyle(
                        color: Colors.black54,
                        fontFamily: "Sarabun",
                        fontSize: 16)),
                SizedBox(
                  height: 15,
                ),
                Card(
                  child: Column(
                    children: [
                      ListTile(
                        title: Text("ชื่อผู้ใช้งาน"),
                        subtitle: Text(Auth.fullName ?? 'ไม่ได้ตั้งค่า'),
                        trailing: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return EditDialog(type: 1);
                                });
                          },
                          child: Text("แก้ไข",
                              style: TextStyle(color: Colors.white)),
                        ),
                      ),
                      ListTile(
                        title: Text("อีเมล์"),
                        subtitle: Text(Auth.email ?? 'เข้าสู่ระบบจากมือถือ'),
                        trailing: true //Auth.email == null
                            ? SizedBox()
                            : RaisedButton(
                                color: Theme.of(context).primaryColor,
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return EditDialog(type: 2);
                                      });
                                },
                                child: Text("แก้ไข",
                                    style: TextStyle(color: Colors.white)),
                              ),
                      ),
                      ListTile(
                        title: Text("เบอร์"),
                        subtitle: Text(
                            Auth.phone ?? 'เข้าสู่ระบบจากบัญชี Social Online'),
                        trailing: Auth.phone == null
                            ? SizedBox()
                            : RaisedButton(
                                color: Theme.of(context).primaryColor,
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return EditDialog(type: 3);
                                      });
                                },
                                child: Text("แก้ไข",
                                    style: TextStyle(color: Colors.white)),
                              ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class EditDialog extends StatefulWidget {
  final int type;

  const EditDialog({Key key, this.type}) : super(key: key);

  @override
  _EditDialogState createState() => _EditDialogState();
}

class _EditDialogState extends State<EditDialog> {
  String text;
  TextEditingController controller = new TextEditingController();
  @override
  void initState() {
    super.initState();
    Auth.context = context;

    switch (widget.type) {
      case 1:
        text = "ชื่อ";
        controller.text = Auth.fullName;
        break;
      case 2:
        text = "อีเมล์";
        controller.text = Auth.email;
        break;
      case 3:
        controller.text = Auth.phone;
        Auth.phoneNumberTxtController.text = Auth.phone;
        text = "เบอร์มือถือ";
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text("แก้ไข$text"),
      content: TextField(
        controller:
            widget.type == 3 ? Auth.phoneNumberTxtController : controller,
        decoration: InputDecoration(helperText: "$text"),
      ),
      actions: [
        MaterialButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text("ยกเลิก")),
        MaterialButton(
            color: Theme.of(context).primaryColor,
            onPressed: () async {
              Map mapAuthData;
              if (widget.type != 3) {
                mapAuthData = {"full_name": controller.text};
                await update(mapAuthData);
              } else {
                if (Auth.phoneNumberTxtController.text == Auth.phone ||
                    "+66" + Auth.phoneNumberTxtController.text.substring(1) ==
                        Auth.phone) {
                  Fluttertoast.showToast(msg: "เลขหมายนี้ถูกลงทะเบียนอยู่แล้ว");
                } else {
                  await Auth.signInWithPhone(isForValidate: true);
                }
              }
            },
            child: Text("บันทึก",
                style: TextStyle(color: Colors.white, fontFamily: "sarabun"))),
      ],
    );
  }

  update(Map mapAuthData) async {
    bool isSuccess = await Auth.updateUser(mapAuthData);

    if (isSuccess) {
      Fluttertoast.showToast(msg: "อัปเดตข้อมูลสำเร็จ!");
      Navigator.of(context).pop();
      Navigator.of(context).pop();
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => UpdateProfile()));
    } else {
      Fluttertoast.showToast(msg: "ไม่สามารถอัปเดตข้อมูลได้");
    }
  }
}
