import '../Config.dart';

class MyNotification {
  int id;
  String title, message, createdAt;
  bool read;
  int farmId;

  static String nextUrl;

  MyNotification(
      {this.id,
      this.title,
      this.message,
      this.read,
      this.farmId,
      this.createdAt});
  static List<MyNotification> notiList = [];

  static getNotifications() async {
    MyNotification.notiList.clear();
    var token = await Config.getToken();
    Map data = {
      "token": token,
    };
    var res = await Config.myPost(data, Config.api['get-notifications']);
    if (res['status']) {
      for (var noti in res['data']['data']) {
        MyNotification.notiList.add(MyNotification(
          id: noti['id'],
          title: noti['title'],
          message: noti['body'],
          farmId: noti['farm_id'],
          read:
              (noti['is_read'] != null && noti['is_read'] == 1) ? true : false,
          createdAt: noti['created_at'] ?? 'ไม่ระบุ',
        ));
      }
      MyNotification.nextUrl = res['next_page_url'];
    }
  }

  static getMoreNotifications() async {
    var token = await Config.getToken();
    Map data = {
      "token": token,
    };
    var res = await Config.myPost(data, MyNotification.nextUrl);
    if (res['status']) {
      for (var noti in res['data']['data']) {
        MyNotification.notiList.add(MyNotification(
          id: noti['id'],
          title: noti['title'],
          message: noti['body'],
          farmId: noti['farm_id'],
          read:
              (noti['is_read'] != null && noti['is_read'] == 1) ? true : false,
          createdAt: noti['created_at'] ?? 'ไม่ระบุ',
        ));
      }
      MyNotification.nextUrl = res['next_page_url'];
    }
  }

  readNoti() async {
    var token = await Config.getToken();
    Map data = {
      "token": token,
      "readNotification": {"notification_id": this.id, "is_read": this.read}
    };
    var res = await Config.myPost(data, Config.api['read-notification']);
    if (res['status']) {
      return true;
    } else {
      return false;
    }
  }
}
