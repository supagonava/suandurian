import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysmartfarm/Config.dart';
import 'package:mysmartfarm/screens/Loading.dart';
import 'package:mysmartfarm/screens/LoginPage.dart';
import 'package:mysmartfarm/screens/MainPage.dart';
import 'package:mysmartfarm/screens/ProfileUpdate.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';

import '../screens/LoginPage.dart';

class Auth {
  static FirebaseAuth auth = FirebaseAuth.instance;
  static BuildContext context;
  static int count = 60;
  static String firebaseUid,
      qrcode,
      fullName,
      phone,
      email,
      createdAt,
      updatedAt;
  static int id;

  static Map preUser = {
    "firebase_uid": null,
    "phone": null,
    "email": null,
    "full_name": null,
    "image": null,
  };

  static updateUser(Map mapAuthData) async {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (BuildContext context) => LoadingScreen()));
    String token = await Config.getToken();
    Map data = {"token": token, "PostUser": mapAuthData};
    var res = await Config.myPost(data, Config.api['auth-update']);
    if (res['status']) {
      var data = res['data']['decoded']['user'];
      await Config.setToken(res['data']['token']);
      Auth.id = data['id'];
      Auth.firebaseUid = data['firebase_uid'];
      Auth.qrcode = data['qrcode'];
      Auth.fullName = data['full_name'];
      Auth.phone = data['phone'];
      Auth.email = data['email'];
      Auth.createdAt = data['create_at'];
      Auth.updatedAt = data['update_at'];
      Navigator.of(context).pop();
      return true;
    }
    Navigator.of(context).pop();
    return false;
  }

  static String phoneVerificationId;
  static var credential;
  static TextEditingController phoneNumberTxtController =
      TextEditingController();
  static TextEditingController smsCodeTxtController = TextEditingController();

  static signIn(int method) async {
    try {
      switch (method) {
        case 1: //phone
          await signInWithPhone(isForValidate: false);
          break;
        case 2: //Google
          await signInWithGoogle();
          break;
        case 3: //facebook
          await signInWithFacebook();
          break;
        case 4: //apple
          await signInWithApple();
          break;
        case 5: //line
          await signInWithLine();
          break;
        default: //nothing
          Config.buildShowDialog(
              context: context,
              message: "เกิดข้อผิดพลาดขณะเลือกเส้นทางการเข้าสู่ระบบ");
      }
    } catch (e) {
      Config.buildShowDialog(context: context, message: "ยกเลิกโดยผู้ใช้ \n$e");
    }
  }

  static signInWithPhone({bool isForValidate}) async {
    String requestPhone = Auth.phoneNumberTxtController.text;
    Fluttertoast.showToast(
        msg: "กำลังส่งรหัสไปยัง ${phoneNumberTxtController.text}");
    if (requestPhone.isNotEmpty) {
      if (requestPhone.startsWith("0")) {
        requestPhone = "+66" + requestPhone.substring(1);
      }
      await FirebaseAuth.instance.verifyPhoneNumber(
          phoneNumber: requestPhone,
          verificationCompleted: (PhoneAuthCredential verifiCredential) async {
            await Fluttertoast.showToast(
                msg:
                    "ส่งรหัสไปยัง ${Auth.phoneNumberTxtController.text} สำเร็จ");
          },
          verificationFailed: (FirebaseAuthException e) {
            Config.buildShowDialog(
                context: context, message: "หมายเลขโทรศัพท์ไม่ถูกต้อง");
          },
          codeSent: (String verificationId, int resendToken) async {
            print(Config.log + "set verificationId to $verificationId");
            String smsCode = await Auth.showInputOPT();
            if (smsCode != null) {
              try {
                print(Config.log + "smsCode =" + smsCode);
                print(Config.log + "verificationId = " + verificationId);
                credential = PhoneAuthProvider.credential(
                    verificationId: verificationId, smsCode: smsCode);
                Auth.phoneNumberTxtController.clear();
                Auth.smsCodeTxtController.clear();
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => LoadingScreen()));
                await Auth.auth.signInWithCredential(credential);
                Navigator.of(context).pop();
                if (!isForValidate) {
                  await Auth.signInToSystem(isLineLogin: false);
                } else {
                  Map mapAuthData = {
                    "firebase_uid": auth.currentUser.uid,
                    "phone": auth.currentUser.phoneNumber
                  };
                  bool updated = await updateUser(mapAuthData);
                  if (updated) {
                    Fluttertoast.showToast(msg: "อัปเดตข้อมูลสำเร็จ!");
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => UpdateProfile()));
                  } else {
                    Navigator.of(context).pop();
                    Fluttertoast.showToast(
                        msg:
                            "ล้มเหลวในการอัปเดตข้อมูล โปรดลองอีกครั้งภายหลัง!");
                  }
                }
              } catch (e) {
                Auth.smsCodeTxtController.clear();
                await Config.buildShowDialog(
                    context: context,
                    message:
                        "รหัสผ่าน OPT ไม่ถูกต้องกรุณาทำรายการใหม่อีกครั้ง");
              }
            }
          },
          codeAutoRetrievalTimeout: (String verificationId) async {
            await Config.buildShowDialog(
                context: context, message: "หมดเวลาทำรายการ");
            Auth.phoneVerificationId = verificationId;
            print(
                Config.log + "timeout\nset verificationId to $verificationId");
          },
          timeout: Duration(seconds: 60));
    } else {
      Config.buildShowDialog(
          context: context, message: "กรุณากรอกหมายเลขโทรศัพท์");
    }
  }

  static signInWithGoogle() async {
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;
    credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    await Auth.auth.signInWithCredential(credential);
    await Auth.signInToSystem(isLineLogin: false);
  }

  static signInWithFacebook() async {
    var result = await FacebookAuth.instance.login();
    credential = FacebookAuthProvider.credential(result.accessToken.token);
    await Auth.auth.signInWithCredential(credential);
    await Auth.signInToSystem(isLineLogin: false);
  }

  static signInWithApple() async {
    final AuthorizationResult result = await AppleSignIn.performRequests([
      AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
    ]);
    switch (result.status) {
      case AuthorizationStatus.authorized:
        print("successfull sign in");
        final AppleIdCredential appleIdCredential = result.credential;
        OAuthProvider oAuthProvider = new OAuthProvider("apple.com");
        final AuthCredential credential = oAuthProvider.credential(
          idToken: String.fromCharCodes(appleIdCredential.identityToken),
          accessToken:
              String.fromCharCodes(appleIdCredential.authorizationCode),
        );

        await auth.signInWithCredential(credential);

        break;
      // here we're going to sign in the user within firebase
      case AuthorizationStatus.error:
        // do something
        break;

      case AuthorizationStatus.cancelled:
        print('User cancelled');
        break;
    }
  }

  static Future<String> showInputOPT() {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: ((BuildContext context) {
          return Card(
            color: Colors.transparent,
            child: AlertDialog(
              title: Text("ป้อนรหัส OTP ภายใน ($count วินาที)"),
              content: TextField(
                maxLength: 6,
                controller: Auth.smsCodeTxtController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintMaxLines: 6, hintText: "รหัส 6 หลักจาก SMS"),
              ),
              actions: [
                MaterialButton(
                  onPressed: () => Navigator.of(context).pop(null),
                  child: Text("ยกเลิก"),
                ),
                MaterialButton(
                  onPressed: () {
                    if (Auth.smsCodeTxtController.text.isNotEmpty &&
                        Auth.smsCodeTxtController.text.length == 6) {
                      Navigator.of(context).pop(Auth.smsCodeTxtController.text);
                    } else {
                      Fluttertoast.showToast(
                          msg: "คุณกรอกรหัสยังไม่ครบ 6 หลัก");
                    }
                  },
                  color: Colors.green,
                  child: Text(
                    "ยืนยัน",
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          );
        }));
  }

  static signInWithLine() async {
    clearPreuser();
    await LineSDK.instance.setup("1655089402").then((_) {
      print("LineSDK is Prepared");
    });
    try {
      final result = await LineSDK.instance.login(scopes: ["profile"]);
      Auth.preUser = {
        "firebase_uid": result.userProfile.userId,
        "phone": null,
        "email": result.userProfile.userId.substring(0, 10) + "@line.auth",
        "full_name": result.userProfile.displayName,
        "image": result.userProfile.pictureUrl,
      };
      print(Auth.preUser);
      await signInToSystem(isLineLogin: true);
    } on PlatformException catch (e) {
      print(e);
      switch (e.code.toString()) {
        case "CANCEL":
          Config.buildShowDialog(
              context: context,
              message:
                  "เมื่อสักครู่คุณกดยกเลิกการเข้าสู่ระบบ กรุณาเข้าสู่ระบบใหม่อีกครั้ง");
          print("User Cancel the login");
          break;
        case "AUTHENTICATION_AGENT_ERROR":
          Config.buildShowDialog(
              context: context,
              message: "คุณไม่อนุญาติการเข้าสู่ระบบด้วย LINE");
          print("User decline the login");
          break;
        default:
          Config.buildShowDialog(
              context: context, message: "เกิดข้อผิดพลาดที่ไม่ทราบสาเหตุ $e");
          print("Unknown but failed to login");
          break;
      }
    }
  }

  static Future getAccessToken() async {
    try {
      final result = await LineSDK.instance.currentAccessToken;
      return result.value;
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  static signInToSystem({bool isLineLogin}) async {
    if (!isLineLogin) {
      clearPreuser();
      preUser = {
        "firebase_uid": Auth.auth.currentUser.uid,
        "phone": Auth.auth.currentUser.phoneNumber,
        "email": Auth.auth.currentUser.email,
        "full_name": Auth.auth.currentUser.displayName,
        "image": Auth.auth.currentUser.photoURL,
      };
    }
    try {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) => LoadingScreen()));
      var res = await Config.myPost(preUser, Config.api['login']);
      if (res['status'] && res['data'] != null) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        await prefs.setString('token', res['data']['token']);
        await validateToken();
      } else {
        Fluttertoast.showToast(msg: "เกิดข้อผิดพลาดขณะ ติดต่อไปยังเซิร์ฟเวอร์");
        Navigator.of(context).pop();
      }
    } catch (e) {
      Navigator.of(context).pop();
    }
  }

  static validateToken() async {
    var token = await Config.getToken();
    if (token != null) {
      Map data = {'token': token};
      var response = await Config.myPost(data, Config.api['validate']);
      if (response['status']) {
        var data = response['data'];
        Auth.id = data['id'];
        Auth.firebaseUid = data['firebase_uid'];
        Auth.qrcode = data['qrcode'];
        Auth.fullName = data['full_name'];
        Auth.phone = data['phone'];
        Auth.email = data['email'];
        Auth.createdAt = data['create_at'];
        Auth.updatedAt = data['update_at'];
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => MainPage()),
            (route) => false);
        return true;
      }
    }
    return false;
  }

  static signOutFromSystem() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
    Auth.updatedAt = null;
    Auth.createdAt = null;
    Auth.email = null;
    Auth.firebaseUid = null;
    Auth.fullName = null;
    Auth.phone = null;
    Auth.qrcode = null;
    await Auth.auth.signOut();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (route) => false);
  }

  static clearPreuser() {
    preUser = {
      "firebase_uid": null,
      "phone": null,
      "email": null,
      "full_name": null,
      "image": null,
    };
  }
}
