import '../Config.dart';

class User {
  var id, fullName, isManager, isOwner, isAdmin, isViewer, phone, email, qrcode;
  static const Map roleName = {
    30: "ผู้สังเกตุการณ์",
    40: "ผู้จัดการฟาร์ม",
    50: "เจ้าของฟาร์ม",
    100: "นักพัฒนาระบบ"
  };

  User(
      {this.id,
      this.fullName,
      this.isManager,
      this.isViewer,
      this.qrcode,
      this.phone,
      this.isAdmin,
      this.isOwner,
      this.email});

  static List<User> userList = [];
  static User localUser;
  
  updateRole(int farmId, int roleId) async {
    var token = await Config.getToken();
    Map data = {
      "token": token,
      "RoleUser": {"user_id": this.id, "role_id": roleId, "farm_id": farmId}
    };
    var res = await Config.myPost(data, Config.api['assingRole']);
    if (res['message'] == null) {
      return true;
    }
    return false;
  }

  clearRole(int farmId) async {
    var token = await Config.getToken();
    Map data = {
      "token": token,
      "RoleUser": {"user_id": this.id, "farm_id": farmId}
    };
    var res = await Config.myPost(data, Config.api['clearRole']);
    if (res['message'] == null) {
      return true;
    }
    return false;
  }
}
