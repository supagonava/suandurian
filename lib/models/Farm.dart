import 'package:fluttertoast/fluttertoast.dart';
import 'package:mysmartfarm/models/Auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Config.dart';
import 'User.dart';

class Farm {
  int id;
  String name;
  double size;
  FarmLatLng location;
  Farm({this.id, this.name, this.size, this.location});

  static List<Farm> farmList = [];

  static Future<void> getFarm() async {
    Farm.farmList.clear();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get('token');
    var data = {"token": token};
    var res = await Config.myPost(data, Config.api['get-farm']);
    if (res["status"]) {
      for (var farm in res['data']['data']) {
        Farm.farmList.add(Farm(
            id: farm['id'],
            name: farm['name'],
            size: double.parse(farm['size'].toString()),
            location: FarmLatLng(
                lat: double.parse(farm['lat'].toString()),
                lng: double.parse(farm['lng'].toString()))));
      }
    }
  }

  getUser() async {
    try {
      User.userList.clear();
      var token = await Config.getToken();
      Map data = {"token": token};
      var res = await Config.myPost(
          data, Config.api['get-user-by-farm'] + this.id.toString());
      if (res['status']) {
        for (var user in res['data']['data']) {
          int originalUser =
              User.userList.indexWhere((element) => element.id == user['id']);
          if (originalUser != -1) {
            switch (user['role_id']) {
              case 30:
                User.userList[originalUser].isViewer = 1;
                break;
              default:
                User.userList[originalUser].isManager = 1;
                User.userList[originalUser].isManager = 1;
                break;
            }
          } else {
            User.userList.add(User(
                id: user['id'],
                email: user['email'],
                phone: user['phone'],
                fullName: user['full_name'],
                qrcode: user['qrcode'],
                isViewer: user['role_id'] == 30 ? 1 : 0,
                isOwner: user['role_id'] == 50 ? 1 : 0,
                isAdmin: user['role_id'] == 100 ? 1 : 0,
                isManager: user['role_id'] == 40 ? 1 : 0));
          }
        }
        
        User.localUser =
            User.userList.firstWhere((element) => element.id == Auth.id);
      }
    } catch (e) {
      Fluttertoast.showToast(msg: "$e");
    }
  }

  createFarm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get('token');
    var data = {
      "token": token,
      "Farm": {
        "name": this.name,
        "size": this.size,
        "lat": this.location.lat,
        "lng": this.location.lng
      }
    };
    var res = await Config.myPost(data, Config.api['create-farm']);
    if (res['status']) {
      return true;
    } else {
      return false;
    }
  }

  delete() async {
    var token = await Config.getToken();
    var data = {"token": token};
    var res = await Config.myPost(
        data, Config.api["delete-farm"] + this.id.toString());
    if (res['status']) {
      return true;
    }
    return false;
  }

  update() async {
    var token = await Config.getToken();
    Map data = {
      "token": token,
      "Farm": {
        "name": this.name,
        "size": this.size,
        "lat": this.location.lat,
        "lng": this.location.lng
      }
    };
    var res = await Config.myPost(
        data, Config.api['update-farm'] + this.id.toString());
    if (res['status']) {
      return true;
    }
    return false;
  }
}

class FarmLatLng {
  double lat;
  double lng;

  FarmLatLng({this.lat, this.lng});
}
