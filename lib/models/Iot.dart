import '../Config.dart';

class IOT {
  int id, farmId;
  String name, variable, unit, status, qrcode;
  double value, min, max;

  IOT(
      {this.id,
      this.status,
      this.name,
      this.variable,
      this.value,
      this.unit,
      this.min,
      this.max,
      this.qrcode,
      this.farmId});

  static List<IOT> iotList = [];

  static getByFarm(farmId) async {
    IOT.iotList.clear();
    String token = await Config.getToken();
    Map data = {"token": token, "farmId": farmId};
    var res = await Config.myPost(data, Config.api['get-devices']);
    if (res['status']) {
      for (var device in res['data']['data']) {
        try {
          IOT.iotList.add(IOT(
              id: device['id'],
              name: device['name'],
              unit: device['unit'],
              variable: device['variable'],
              status: device['status'],
              farmId: device['farm_id'],
              qrcode: device['qrcode'],
              min: double.parse(device['min_suggest'].toString()),
              max: double.parse(device['max_suggest'].toString()),
              value: double.parse(device['current_value'].toString())));
        } catch (e) {
          print(e);
        }
      }
    }
  }

  update() async {
    String token = await Config.getToken();
    Map data = {
      "token": token,
      "Device": {
        "min_suggest": this.min,
        "max_suggest": this.max,
        "status": this.status,
        "farm_id": this.farmId
      }
    };
    var res = await Config.myPost(
        data, Config.api['update-device'] + this.id.toString());
    if (res['status']) {
      return true;
    }
    return false;
  }
}
