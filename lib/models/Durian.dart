import 'dart:io';

import 'package:mysmartfarm/Config.dart';

class Durian {
  int id, farmId;
  String name, imageUrl, createdAt;
  File image;
  Durian(
      {this.id,
      this.imageUrl,
      this.image,
      this.name,
      this.farmId,
      this.createdAt});

  static List<Durian> durianList = [];

  static getDurians(int farmId) async {
    durianList.clear();
    var token = await Config.getToken();
    Map data = {"token": token, "farm_id": farmId};
    var res = await Config.myPost(data, Config.api['durian-get']);
    if (res['status']) {
      var data = res['data']['data'];
      for (var durian in data) {
        durianList.add(Durian(
            id: durian['id'],
            createdAt: durian['created_at'],
            farmId: durian['farm_id'],
            imageUrl: Config.storagePath + durian['image'],
            name: durian['name']));
      }
    }
  }

  Future<bool> save({bool isUpdate}) async {
    String token = await Config.getToken();
    Map data = {
      "token": token,
    };
    if (!isUpdate) {
      data.addAll({
        "Durian": {
          "farm_id": this.farmId,
          "name": this.name,
          "image": this.image,
        }
      });
    } else {
      if (this.image == null) {
        data.addAll({
          "Durian": {"id": this.id, "farm_id": this.farmId, "name": this.name}
        });
      } else {
        data.addAll({
          "Durian": {
            "id": this.id,
            "farm_id": this.farmId,
            "name": this.name,
            "image": this.image,
          }
        });
      }
    }
    var res = await Config.myMultipartReq(
        url: Config.api[isUpdate ? "durian-update" : "durian-store"],
        data: data);
    if (res['status']) {
      await getDurians(this.farmId);
      return true;
    }
    return false;
  }

  Future<bool> delete() async {
    var token = await Config.getToken();
    var data = {"token": token, "durian_id": this.id};
    var res = await Config.myPost(data, Config.api['delete-durian']);
    if (res['status']) {
      return true;
    }
    return false;
  }
}

class DurianDetails {
  int id, durianId;
  String title, content, createdAt, updatedAt, date, imageUrl;
  File image;
  DurianDetails(
      {this.id,
      this.title,
      this.content,
      this.createdAt,
      this.updatedAt,
      this.imageUrl,
      this.date,
      this.image,
      this.durianId});

  static List<DurianDetails> details = [];

  static getDetails(int durianId) async {
    details.clear();
    var token = await Config.getToken();
    Map data = {"token": token, "durian_id": durianId};
    var res = await Config.myPost(data, Config.api['detail-get']);
    if (res['status']) {
      var data = res['data']['data'];
      for (var detail in data) {
        details.add(DurianDetails(
            id: detail['id'],
            createdAt: detail['created_at'],
            content: detail['detail'],
            title: detail['title'],
            date: detail['date'],
            durianId: detail['durian_id'],
            imageUrl: Config.storagePath + detail['image']));
      }
    }
  }

  save({bool isUpdate}) async {
    var token = await Config.getToken();
    Map data = {"token": token};
    var res;
    if (!isUpdate) {
      data.addAll({
        "detail": {
          "title": this.title,
          "image": this.image,
          "detail": this.content,
          "date": this.date,
          "durian_id": this.durianId
        }
      });
      res = await Config.myMultipartReq(
          data: data, url: Config.api['durian-detail-store']);
    } else {
      if (this.image == null) {
        data.addAll({
          "detail": {
            "id": this.id,
            "title": this.title,
            "detail": this.content,
            "date": this.date,
            "durian_id": this.durianId
          }
        });
      } else {
        data.addAll({
          "detail": {
            "id": this.id,
            "title": this.title,
            "image": this.image,
            "detail": this.content,
            "date": this.date,
            "durian_id": this.durianId
          }
        });
      }
      res = await Config.myMultipartReq(
          data: data, url: Config.api['durian-detail-update']);
    }
    if (res['status']) {
      return true;
    }
    return false;
  }

  Future<bool> delete() async {
    var token = await Config.getToken();
    var data = {"token": token, "detail_id": this.id};
    var res = await Config.myPost(data, Config.api['delete-durian-detail']);
    if (res['status']) {
      return true;
    }
    return false;
  }
}
