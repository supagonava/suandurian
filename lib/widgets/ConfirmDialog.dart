import 'package:flutter/material.dart';

class ConfirmDialog extends StatelessWidget {
  final BuildContext context;
  ConfirmDialog(this.context);
  @override
  Widget build(context) {
    return AlertDialog(
      title: Text("แจ้งเตือน"),
      content: Text("ยืนยันที่จะดำเนินการ"),
      actions: [
        MaterialButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: Text("ยกเลิก")),
        MaterialButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: Text("ตกลง"),
        )
      ],
    );
  }
}
