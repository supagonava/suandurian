import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyFarmerListbox extends StatefulWidget {
  final List originalList;

  MyFarmerListbox({@required this.originalList});
  @override
  _MyFarmerListboxState createState() => _MyFarmerListboxState(originalList);
}

class _MyFarmerListboxState extends State<MyFarmerListbox> {
  List originalList, cloneList;
  String txtFieldString = "ค้นหาเกษตรกร";
  TextEditingController txtController = TextEditingController();
  _MyFarmerListboxState(originalList) {
    this.originalList = originalList;
    cloneList = originalList;
  }

  @override
  Widget build(BuildContext context) {
    return farmerListBox(context);
  }

  Container farmerListBox(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 35, vertical: 20),
      height: 400,
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        borderRadius: BorderRadius.circular(18.00),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: searchTextField(),
            padding: EdgeInsets.all(10),
          ),
          Flexible(
            flex: 5,
            child: GridView.count(
              crossAxisCount: 3,
              scrollDirection: Axis.vertical,
              children: originalList.toString() != "null"
                  ? List.generate(originalList.length, (index) {
                      return InkWell(
                        onTap: () {
                          setFarmerToView(originalList[index]
                                  ["farmer_username"]
                              .toString());
                          Navigator.of(context)
                              .pushNamed('/ViewFarmerDetail');
                        },
                        child: Container(
                          height: 130,
                          child: Column(
                            children: <Widget>[
                              Container(
                                height: 75.00,
                                width: 75.00,
                                decoration: BoxDecoration(
                                  color: originalList[index]["is_iot_manager"]
                                              .toString() ==
                                          '1'
                                      ? Color(0xfffecb59)
                                      : Color(0xff25ced1), // ใส่สีวงกลมที่นี่
                                  image: DecorationImage(
                                    image: AssetImage(
                                        "assets/images/farmerLogo.png"),
                                  ),
                                  shape: BoxShape.circle,
                                ),
                              ),
                              Text(
                                originalList[index]["farmer_name"]
                                        .toString() +
                                    '  ' +
                                    originalList[index]["farmer_lastname"]
                                        .toString(),
                                style: TextStyle(
                                    fontFamily: "Sarabun",
                                    color: Color(0xff707070)),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        ),
                      );
                    })
                  : <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            "ไม่มีเกษตรกร",
                            style: TextStyle(
                                fontFamily: "Sarabun",
                                fontSize: 16,
                                color: Colors.blueGrey),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 15, bottom: 5),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.account_box,
                      color: Color(0xfffed16c),
                    ),
                    Text(
                      "ผู้จัดการฟาร์ม",
                      style: TextStyle(
                        fontFamily: "Sarabun",
                        fontSize: 16,
                        color: Color(0xfffed16c),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.account_box,
                      color: Color(0xff36d2d4),
                    ),
                    Text(
                      "ผู้สังเกตุการณ์ฟาร์ม",
                      style: TextStyle(
                        fontFamily: "Sarabun",
                        fontSize: 16,
                        color: Color(0xff36d2d4),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Container searchTextField() {
    return Container(
      child: TextField(
        controller: txtController,
        onChanged: (txtFieldString) {
          setState(() {
            originalList = cloneList
                .where((element) => ((element['farmer_name'].toString() +
                        ' ' +
                        (element['farmer_lastname'].toString()))
                    .toLowerCase()
                    .contains(txtFieldString.toString().toLowerCase())))
                .toList();
          });
        },
        decoration: InputDecoration(
          hintText: txtFieldString,
          suffixIcon: Icon(Icons.search),
        ),
        style: TextStyle(
          fontFamily: "Sarabun",
        ),
      ),
    );
  }

  Future<void> setFarmerToView(String farmerUname) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("farmerUname", farmerUname);
  }
}
