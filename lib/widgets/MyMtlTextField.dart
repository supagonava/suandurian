import 'package:flutter/material.dart';

class MyMltTextField extends StatelessWidget {
  final String txtFieldString;
  final TextInputType inputType;
  final TextEditingController txtController = TextEditingController();
  MyMltTextField({@required this.txtFieldString, @required this.inputType});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      child: Container(

        padding: EdgeInsets.only(top:10,left:10),
        child: TextFormField(
          controller: txtController,
          obscureText: false,
          maxLines: 3,
          decoration: InputDecoration.collapsed(
            hintText: txtFieldString,
          ),
          keyboardType: inputType,
          style: TextStyle(
            fontFamily: "Sarabun",
          ),
        ),
      ),
    );
  }
}
