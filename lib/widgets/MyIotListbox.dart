import 'package:flutter/material.dart';
import 'package:mysmartfarm/models/Iot.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyIotListbox extends StatefulWidget {
  @override
  _MyIotListboxState createState() => _MyIotListboxState();
}

class _MyIotListboxState extends State<MyIotListbox> {
  String txtFieldString = "ค้นหาอุปกรณ์";
  TextEditingController txtController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return iotListBox(context);
  }

  Container iotListBox(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 35, vertical: 20),
      height: 400,
      width: MediaQuery.of(context).size.width * 0.8,
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        borderRadius: BorderRadius.circular(18.00),
      ),
      child: Column(
        children: <Widget>[
          Container(
            child: searchTextField(),
            padding: EdgeInsets.all(10),
          ),
          Flexible(
            flex: 5,
            child: Container(
              height: 400,
              alignment: Alignment.center,
              child: GridView.count(
                  crossAxisCount: 3,
                  scrollDirection: Axis.vertical,
                  children: List.generate(IOT.iotList.length, (index) {
                    return InkWell(
                      onTap: () {},
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 75.00,
                            width: 75.00,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage("assets/images/iotLogo.png"),
                              ),
                              shape: BoxShape.circle,
                            ),
                          ),
                          Text(
                            IOT.iotList[index].name,
                            style: TextStyle(
                                fontFamily: "Sarabun",
                                color: Color(0xff707070)),
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    );
                  })),
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 15, bottom: 5),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(
                      Icons.important_devices,
                      color: Color(0xff72cb49),
                    ),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      "เปิดและใช้วัดค่า",
                      style: TextStyle(
                        fontFamily: "Sarabun",
                        fontSize: 16,
                        color: Color(0xff72cb49),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(Icons.power_input,
                        color: Colors.blue[400].withOpacity(0.7)),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      "เปิดแต่ไม่ใช้วัดค่า",
                      style: TextStyle(
                          fontFamily: "Sarabun",
                          fontSize: 16,
                          color: Colors.blue[400].withOpacity(0.7)),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Icon(Icons.power, color: Color(0xff707070)),
                    SizedBox(
                      width: 15,
                    ),
                    Text(
                      "ปิด",
                      style: TextStyle(
                          fontFamily: "Sarabun",
                          fontSize: 16,
                          color: Color(0xff707070)),
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container searchTextField() {
    return Container(
      child: TextField(
        controller: txtController,
        onChanged: (txtFieldString) {},
        decoration: InputDecoration(
          hintText: txtFieldString,
          suffixIcon: Icon(Icons.search),
        ),
        style: TextStyle(
          fontFamily: "Sarabun",
        ),
      ),
    );
  }

  setIot(var iot) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("iotJson", iot);
    print(prefs.getString("iotJson"));
  }
}
