import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  final String txtFieldString;
  final TextInputType inputType;
  final TextEditingController txtController = TextEditingController();

  MyTextField({@required this.txtFieldString, @required this.inputType});

  @override
  Widget build(BuildContext context) {
    if (txtFieldString.contains("ค่าเริ่มต้น") ||
        txtFieldString.contains('ค่าสิ้นสุด')) {
      return TextField(
        controller: txtController,
        obscureText: false,
        decoration: InputDecoration(
          filled: true,
          hintText: "$txtFieldString",
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
        ),
        keyboardType: TextInputType.number,
        style: TextStyle(
          fontFamily: "Sarabun",
        ),
      );
    } else {
      return TextField(
        controller: txtController,
        obscureText: txtFieldString.contains("รหัสผ่าน") ? true : false,
        decoration: InputDecoration(
          filled: true,
          hintText: "$txtFieldString",
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(25.7),
          ),
        ),
        keyboardType: inputType,
        style: TextStyle(
          fontFamily: "Sarabun",
        ),
      );
    }
  }
}
