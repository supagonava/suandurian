import 'package:flutter/material.dart';
import '../models/Farm.dart';
import '../screens/DashBoardPage.dart';

class MyFarmListbox extends StatefulWidget {
  @override
  _MyFarmListboxState createState() => _MyFarmListboxState();
}

class _MyFarmListboxState extends State<MyFarmListbox> {
  var size;
  double itemHeight;
  double itemWidth;
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    itemHeight = (size.height - kToolbarHeight - 24) / 2;
    itemWidth = size.width / 2;

    return farmlistBox(context);
  }

  Container farmlistBox(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      height: 400,
      margin: EdgeInsets.only(top: 35, left: 35, right: 35, bottom: 25),
      decoration: BoxDecoration(
        color: Color(0xffffffff),
        borderRadius: BorderRadius.circular(18.00),
      ),
      child: Container(
        alignment: Alignment.center,
        child: Farm.farmList.length == 0
            ? Text("ไม่พบสวนทุเรียนในการดูแลของท่าน",
                style: TextStyle(color: Colors.black, fontFamily: "sarabun"))
            : GridView.count(
                childAspectRatio: (itemWidth / itemHeight),
                crossAxisCount: 3,
                scrollDirection: Axis.vertical,
                children: List.generate(Farm.farmList.length, (index) {
                  return InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) =>
                              DashboardPage(Farm.farmList[index])));
                    },
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 75.00,
                          width: 75.00,
                          decoration: BoxDecoration(
                            color: Color(0xffffffff),
                            image: DecorationImage(
                              image: AssetImage("assets/images/farmlogo.png"),
                              fit: BoxFit.contain,
                            ),
                            border: Border.all(
                              width: 2.00,
                              color: Color(0xffc7d3af),
                            ),
                            shape: BoxShape.circle,
                          ),
                        ),
                        Text(
                          Farm.farmList[index].name,
                          style: TextStyle(
                              fontFamily: "Sarabun", color: Color(0xff707070)),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  );
                })),
      ),
    );
  }
}
