import 'package:flutter/material.dart';

class MyBtn extends StatelessWidget {
  final String btnTxt;
  final Color borderColor;
  final Color btnColor;
  final Color txtColor;
  final Function fn;

  MyBtn({
    @required this.btnTxt,
    @required this.txtColor,
    @required this.btnColor,
    @required this.borderColor,
    @required this.fn,
  });

  @override
  Widget build(BuildContext context) {
  
    return new Container(
      
        height: 50,
        width: double.infinity,
        decoration: BoxDecoration(
          color: btnColor,
          border: Border.all(
            width: 2.00,
            color: borderColor,
          ),
          boxShadow: [
            BoxShadow(
              offset: Offset(0.00, 3.00),
              color: Color(0xff000000).withOpacity(0.16),
              blurRadius: 6,
            ),
          ],
          borderRadius: BorderRadius.circular(9.00),
        ),
        child: FlatButton(
          
            onPressed: () => fn(),
            child: Text(
              btnTxt,
              style: TextStyle(
                color: txtColor,
                fontFamily: "Sarabun",
                fontSize: 16,
              ),
            )));
  }
}
