import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';

class MyFlushBar extends StatelessWidget {
  final String text;

  MyFlushBar({@required this.text});

  @override
  Widget build(BuildContext context) {
    return Flushbar(
      title: 'แจ้งเตือน',
      message: text,
      icon: Icon(
        Icons.info_outline,
        size: 28,
        color: Color(0xff75C46B),
      ),
      leftBarIndicatorColor: Color(0xff75C46B),
      duration: Duration(seconds: 3),
    )..show(context);
  }
}
